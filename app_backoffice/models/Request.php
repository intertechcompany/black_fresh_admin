<?php
class Request extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getNewRequestsTotal()
    {
        $total_requests = Yii::app()->db
            ->createCommand("SELECT COUNT(*) FROM request WHERE is_new = 1")
            ->queryScalar();

        return $total_requests;
    }

    public function getRequestsAdminTotal($per_page = 10)
    {
        $func_args = func_get_args();

        $search_sql = array();
        $bind_sql = array();

        if (!empty($func_args[1])) {
            $request_id = (int) $func_args[1];
            $email = $func_args[1];
            $name = addcslashes($func_args[1], '%_');

            $search_sql[] = '(request_id = :id OR first_name LIKE :name OR last_name LIKE :name OR email = :email)';

            $bind_sql[] = array(
                'param' => ':id',
                'value' => $request_id,
                'type'  => PDO::PARAM_INT,
            );

            $bind_sql[] = array(
                'param' => ':email',
                'value' => $email,
                'type'  => PDO::PARAM_STR,
            );

            $bind_sql[] = array(
                'param' => ':name',
                'value' => $name,
                'type'  => PDO::PARAM_STR,
            );
        }

        $timezone = new DateTimeZone(Yii::app()->timeZone);

        if (!empty($func_args[2]) && CDateTimeParser::parse($func_args[2], 'dd.MM.yyyy') !== false && !empty($func_args[3]) && CDateTimeParser::parse($func_args[3], 'dd.MM.yyyy') !== false) {
            $search_sql[] = '(created >= :date_from AND created < :date_to)';

            // prepare dates
            $date_from = DateTime::createFromFormat('d.m.Y', $func_args[2], $timezone);
            $date_to = DateTime::createFromFormat('d.m.Y', $func_args[3], $timezone);
            $date_to->add(new DateInterval('P1D'));

            $bind_sql[] = array(
                'param' => ':date_from',
                'value' => $date_from->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );

            $bind_sql[] = array(
                'param' => ':date_to',
                'value' => $date_to->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );
        }
        elseif (!empty($func_args[2]) && CDateTimeParser::parse($func_args[2], 'dd.MM.yyyy') !== false) {
            $search_sql[] = 'created >= :date_from';

            // prepare date
            $date_from = DateTime::createFromFormat('d.m.Y', $func_args[2], $timezone);

            $bind_sql[] = array(
                'param' => ':date_from',
                'value' => $date_from->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );
        }
        elseif (!empty($func_args[3]) && CDateTimeParser::parse($func_args[3], 'dd.MM.yyyy') !== false) {
            $search_sql[] = 'created < :date_to';

            // prepare date
            $date_to = DateTime::createFromFormat('d.m.Y', $func_args[3], $timezone);
            $date_to->add(new DateInterval('P1D'));

            $bind_sql[] = array(
                'param' => ':date_to',
                'value' => $date_to->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );
        }

        if (!empty($search_sql)) {
            $total_requests_db = Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM request WHERE " . implode(' AND ', $search_sql));

            foreach ($bind_sql as $bind) {
                $total_requests_db->bindValue($bind['param'], $bind['value'], $bind['type']);
            }

            $total_requests = $total_requests_db->queryScalar();
        }
        else {
            $total_requests = Yii::app()->db
                ->createCommand("SELECT COUNT(*) FROM request")
                ->queryScalar();
        }

        return array(
            'total' => (int) $total_requests,
            'pages' => ceil($total_requests / $per_page),
        );
    }

    public function getRequestsAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
    {
        switch ($sort) {
            case 'request_id':
                $request_by = ($direction == 'asc') ? 'r.request_id' : 'r.request_id DESC';
                break;
            case 'created':
                $request_by = ($direction == 'asc') ? 'r.created' : 'r.created DESC';
                break;
            default:
                $request_by = 'r.request_id DESC';
        }

        $func_args = func_get_args();

        $search_sql = array();
        $bind_sql = array();

        if (!empty($func_args[4])) {
            $request_id = (int) $func_args[4];
            $email = $func_args[4];
            $name = addcslashes($func_args[4], '%_');

            $search_sql[] = '(r.request_id = :id OR r.first_name LIKE :name OR r.last_name LIKE :name OR r.email = :email)';

            $bind_sql[] = array(
                'param' => ':email',
                'value' => $email,
                'type'  => PDO::PARAM_STR,
            );

            $bind_sql[] = array(
                'param' => ':id',
                'value' => $request_id,
                'type'  => PDO::PARAM_INT,
            );

            $bind_sql[] = array(
                'param' => ':name',
                'value' => $name,
                'type'  => PDO::PARAM_STR,
            );
        }

        $timezone = new DateTimeZone(Yii::app()->timeZone);

        if (!empty($func_args[5]) && CDateTimeParser::parse($func_args[5], 'dd.MM.yyyy') !== false && !empty($func_args[6]) && CDateTimeParser::parse($func_args[6], 'dd.MM.yyyy') !== false) {
            $search_sql[] = '(r.created >= :date_from AND r.created < :date_to)';

            // prepare dates
            $date_from = DateTime::createFromFormat('d.m.Y', $func_args[5], $timezone);
            $date_to = DateTime::createFromFormat('d.m.Y', $func_args[6], $timezone);
            $date_to->add(new DateInterval('P1D'));

            $bind_sql[] = array(
                'param' => ':date_from',
                'value' => $date_from->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );

            $bind_sql[] = array(
                'param' => ':date_to',
                'value' => $date_to->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );
        }
        elseif (!empty($func_args[5]) && CDateTimeParser::parse($func_args[5], 'dd.MM.yyyy') !== false) {
            $search_sql[] = 'r.created >= :date_from';

            // prepare date
            $date_from = DateTime::createFromFormat('d.m.Y', $func_args[5], $timezone);

            $bind_sql[] = array(
                'param' => ':date_from',
                'value' => $date_from->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );
        }
        elseif (!empty($func_args[6]) && CDateTimeParser::parse($func_args[6], 'dd.MM.yyyy') !== false) {
            $search_sql[] = 'r.created < :date_to';

            // prepare date
            $date_to = DateTime::createFromFormat('d.m.Y', $func_args[6], $timezone);
            $date_to->add(new DateInterval('P1D'));

            $bind_sql[] = array(
                'param' => ':date_to',
                'value' => $date_to->format('Y-m-d 00:00:00'),
                'type'  => PDO::PARAM_STR,
            );
        }

        if (!empty($search_sql)) {
            $requests_db = Yii::app()->db
                ->createCommand("SELECT r.*, cl.course_name 
                                 FROM request as r 
                                 LEFT JOIN course as c 
                                 ON r.course_id = c.course_id 
                                 LEFT JOIN course_lang as cl 
                                 ON c.course_id = cl.course_id AND cl.language_code = :code
                                 WHERE " . implode(' AND ', $search_sql) . " 
                                 ORDER BY " . $request_by . " 
                                 LIMIT ".$offset.",".$per_page);

            $requests_db->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR);
 
            foreach ($bind_sql as $bind) {
                $requests_db->bindValue($bind['param'], $bind['value'], $bind['type']);
            }

            $requests = $requests_db->queryAll();
        }
        else {
            $requests = Yii::app()->db
                ->createCommand("SELECT r.*, cl.course_name 
                                 FROM request as r 
                                 LEFT JOIN course as c 
                                 ON r.course_id = c.course_id 
                                 LEFT JOIN course_lang as cl 
                                 ON c.course_id = cl.course_id AND cl.language_code = :code 
                                 ORDER BY " . $request_by . " 
                                 LIMIT ".$offset.",".$per_page)
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->queryAll();
        }

        return $requests;
    }

    public function getUserRequests($user_id)
    {
        $requests = Yii::app()->db
            ->createCommand("SELECT * FROM request WHERE user_id = :user_id ORDER BY request_id DESC")
            ->bindValue(':user_id', (int) $user_id, PDO::PARAM_INT)
            ->queryAll();

        return $requests;
    }

    public function getRequestByIdAdmin($id)
    {
        $request = Yii::app()->db
            ->createCommand("SELECT r.*, cl.course_name 
                             FROM request as r 
                             LEFT JOIN course as c 
                             ON r.course_id = c.course_id 
                             LEFT JOIN course_lang as cl 
                             ON c.course_id = cl.course_id AND cl.language_code = :code 
                             WHERE r.request_id = :id LIMIT 1")
            ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
            ->bindValue(':id', (int) $id, PDO::PARAM_INT)
            ->queryRow();

        return $request;
    }

    public function save($model)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        // skip unnecessary attributes
        $skip_attributes = array(
            'request_id',
            'is_new',
        );

        // integer attributes
        $int_attributes = array();

        // date attributes
        $date_attributes = array();

        // delete attributes
        $del_attributes = array();

        // photos attributes
        $save_images = array();

        if (empty($model->request_id)) {
            // insert request
            $insert_request = array(
                'created' => $today,
                'saved' => $today,
            );

            foreach ($model as $field => $value) {
                if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
                    continue;
                }
                elseif (in_array($field, $int_attributes)) {
                    $insert_request[$field] = (int) $value;
                }
                elseif (in_array($field, $date_attributes)) {
                    $date = new DateTime($value);
                    $insert_request[$field] = $date->format('Y-m-d');
                }
                else {
                    $insert_request[$field] = $value;
                }
            }

            try {
                $rs = $builder->createInsertCommand('request', $insert_request)->execute();

                if ($rs) {
                    $request_id = (int) Yii::app()->db->getLastInsertID();

                    return $request_id;
                }
            }
            catch (CDbException $e) {
                // ...
            }
        }
        else {
            // get request data before save
            $request_before = $this->getRequestByIdAdmin($model->request_id);

            $update_request = array(
                'saved' => $today,
            );

            foreach ($model as $field => $value) {
                if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
                    continue;
                }
                elseif (in_array($field, $int_attributes)) {
                    $update_request[$field] = (int) $value;
                }
                elseif (in_array($field, $date_attributes)) {
                    $date = new DateTime($value);
                    $update_request[$field] = $date->format('Y-m-d');
                }
                else {
                    $update_request[$field] = $value;
                }
            }

            $update_criteria = new CDbCriteria(
                array(
                    "condition" => "request_id = :request_id" ,
                    "params" => array(
                        "request_id" => $model->request_id,
                    )
                )
            );

            try {
                $rs = $builder->createUpdateCommand('request', $update_request, $update_criteria)->execute();

                if ($rs) {
                    return true;
                }
            }
            catch (CDbException $e) {
                // ...
            }
        }

        return false;
    }

    public function toggleNew($request_id)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $today = date('Y-m-d H:i:s');

        $update_request = array(
            'saved' => $today,
            'is_new' => 0,
        );

        $update_criteria = new CDbCriteria(
            array(
                "condition" => "request_id = :request_id" ,
                "params" => array(
                    "request_id" => $request_id,
                )
            )
        );

        try {
            $rs = $builder->createUpdateCommand('request', $update_request, $update_criteria)->execute();

            if ($rs) {
                return true;
            }
        }
        catch (CDbException $e) {
            // ...
        }

        return false;
    }

    public function delete($request_id)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $delete_criteria = new CDbCriteria(
            array(
                "condition" => "request_id = :request_id" ,
                "params" => array(
                    "request_id" => $request_id,
                )
            )
        );

        try {
            $rs = $builder->createDeleteCommand('request', $delete_criteria)->execute();

            if ($rs) {
                return true;
            }
        }
        catch (CDbException $e) {
            // ...
        }

        return false;
    }
}
