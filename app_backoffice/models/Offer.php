<?php
class Offer extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getNewOffersTotal()
	{
		$total_offers = Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM offer WHERE is_new = 1")
			->queryScalar();

		return $total_offers;
	}

	public function getOffersAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		$search_sql = array();
		$bind_sql = array();

		if (!empty($func_args[1])) {
			$offer_id = (int) $func_args[1];
			$email = $func_args[1];
			$keyword_plus = '+' . preg_replace('#\s+#u', ' +', trim($func_args[1]));
			$keyword_quoted = '"' . trim($func_args[1]) . '"';
			$keyword = '(' . $keyword_plus . ') (' . $keyword_quoted . ')';

			$search_sql[] = '(offer_id = :id OR MATCH (full_name) AGAINST (:keyword IN BOOLEAN MODE) OR email = :email)';
			
			$bind_sql[] = array(
				'param' => ':id',
				'value' => $offer_id,
				'type'  => PDO::PARAM_INT,
			);

			$bind_sql[] = array(
				'param' => ':email',
				'value' => $email,
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':keyword',
				'value' => $keyword,
				'type'  => PDO::PARAM_STR,
			);
		}

		$timezone = new DateTimeZone(Yii::app()->timeZone);

		if (!empty($func_args[2]) && CDateTimeParser::parse($func_args[2], 'dd.MM.yyyy') !== false && !empty($func_args[3]) && CDateTimeParser::parse($func_args[3], 'dd.MM.yyyy') !== false) {
			$search_sql[] = '(created >= :date_from AND created < :date_to)';

			// prepare dates
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[2], $timezone);
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[3], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[2]) && CDateTimeParser::parse($func_args[2], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created >= :date_from';

			// prepare date
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[2], $timezone);

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[3]) && CDateTimeParser::parse($func_args[3], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created < :date_to';

			// prepare date
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[3], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($search_sql)) {
			$total_offers_db = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM offer WHERE " . implode(' AND ', $search_sql));
				
			foreach ($bind_sql as $bind) {
				$total_offers_db->bindValue($bind['param'], $bind['value'], $bind['type']);
			}
			
			$total_offers = $total_offers_db->queryScalar();
		}
		else {
			$total_offers = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM offer")
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_offers,
			'pages' => ceil($total_offers / $per_page),
		);
	}

	public function getOffersAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'offer_id':
				$offer_by = ($direction == 'asc') ? 'offer_id' : 'offer_id DESC';
				break;
			case 'created':
				$offer_by = ($direction == 'asc') ? 'created' : 'created DESC';
				break;
			default:
				$offer_by = 'offer_id DESC';
		}

		$func_args = func_get_args();

		$search_sql = array();
		$bind_sql = array();

		if (!empty($func_args[4])) {
			$offer_id = (int) $func_args[4];
			$email = $func_args[4];
			$keyword_plus = '+' . preg_replace('#\s+#u', ' +', trim($func_args[4]));
			$keyword_quoted = '"' . trim($func_args[1]) . '"';
			$keyword = '(' . $keyword_plus . ') (' . $keyword_quoted . ')';

			$search_sql[] = '(offer_id = :id OR MATCH (full_name) AGAINST (:keyword IN BOOLEAN MODE) OR email = :email)';
			
			$bind_sql[] = array(
				'param' => ':email',
				'value' => $email,
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':id',
				'value' => $offer_id,
				'type'  => PDO::PARAM_INT,
			);

			$bind_sql[] = array(
				'param' => ':keyword',
				'value' => $keyword,
				'type'  => PDO::PARAM_STR,
			);
		}

		$timezone = new DateTimeZone(Yii::app()->timeZone);

		if (!empty($func_args[5]) && CDateTimeParser::parse($func_args[5], 'dd.MM.yyyy') !== false && !empty($func_args[6]) && CDateTimeParser::parse($func_args[6], 'dd.MM.yyyy') !== false) {
			$search_sql[] = '(created >= :date_from AND created < :date_to)';

			// prepare dates
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[5], $timezone);
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[6], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[5]) && CDateTimeParser::parse($func_args[5], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created >= :date_from';

			// prepare date
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[5], $timezone);

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[6]) && CDateTimeParser::parse($func_args[6], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created < :date_to';

			// prepare date
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[6], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($search_sql)) {
			$offers_db = Yii::app()->db
				->createCommand("SELECT * FROM offer WHERE " . implode(' AND ', $search_sql) . " ORDER BY " . $offer_by . " LIMIT ".$offset.",".$per_page);
			
			foreach ($bind_sql as $bind) {
				$offers_db->bindValue($bind['param'], $bind['value'], $bind['type']);
			}
			
			$offers = $offers_db->queryAll();
		}
		else {
			$offers = Yii::app()->db
				->createCommand("SELECT * FROM offer ORDER BY " . $offer_by . " LIMIT ".$offset.",".$per_page)
				->queryAll();
		}
			
		return $offers;
	}

	public function getUserOffers($user_id)
	{
		$offers = Yii::app()->db
			->createCommand("SELECT * FROM offer WHERE user_id = :user_id ORDER BY offer_id DESC")
			->bindValue(':user_id', (int) $user_id, PDO::PARAM_INT)
			->queryAll();

		return $offers;
	}

	public function getOfferByIdAdmin($id)
	{
		$offer = Yii::app()->db
			->createCommand("SELECT * FROM offer WHERE offer_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();
			
		return $offer;
	}

	public function getOfferProducts($offer_id)
	{
		$offer_products = Yii::app()->db
			->createCommand("SELECT oi.*, p.product_alias, p.product_sku, p.product_price_type, p.product_pack_size, p.product_photo, p.product_price, pl.product_title 
							 FROM offer_item as oi 
							 LEFT JOIN product as p 
							 ON oi.product_id = p.product_id 
							 LEFT JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE oi.offer_id = :offer_id 
							 ORDER BY oi.offer_item_id")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->bindValue(':offer_id', (int) $offer_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($offer_products)) {
			$variant_ids = array_filter(array_column($offer_products, 'variant_id'), function($v) {
    			return $v > 0;
			});

			if (!empty($variant_ids)) {
				$variants = Product::model()->getVariantsByIds($variant_ids);

				foreach ($offer_products as $key => $offer_product) {
					$offer_products[$key]['variant'] = array();
					$variant_id = $offer_product['variant_id'];

					if ($variant_id && isset($variants[$variant_id])) {
						$offer_products[$key]['variant'] = $variants[$variant_id];
					}
				}
			}
		}
			
		return $offer_products;
	}

	public function save($model)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'offer_id',
			'is_new',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		if (empty($model->offer_id)) {
			// insert offer
			$insert_offer = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_offer[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$insert_offer[$field] = $date->format('Y-m-d');
				}
				else {
					$insert_offer[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('offer', $insert_offer)->execute();

				if ($rs) {
					$offer_id = (int) Yii::app()->db->getLastInsertID();

					return $offer_id;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_offer = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_offer[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$update_offer[$field] = $date->format('Y-m-d');
				}
				else {
					$update_offer[$field] = $value;
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "offer_id = :offer_id" , 
					"params" => array(
						"offer_id" => $model->offer_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('offer', $update_offer, $update_criteria)->execute();

				if ($rs) {
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function toggleNew($offer_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_offer = array(
			'saved' => $today,
			'is_new' => 0,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "offer_id = :offer_id" , 
				"params" => array(
					"offer_id" => $offer_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('offer', $update_offer, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($offer_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "offer_id = :offer_id" , 
				"params" => array(
					"offer_id" => $offer_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('offer', $delete_criteria)->execute();

			if ($rs) {
				$builder->createDeleteCommand('offer_item', $delete_criteria)->execute();

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function generateOffer($offer, $products, $seller)
	{
		$current_lang = Yii::app()->language;
		Yii::app()->language = 'fi';

		require_once Yii::getPathOfAlias('application.vendor.Mpdf') . DS . 'autoload.php';

		$html = Yii::app()->getController()->renderPartial('//pdf/offer', array('offer' => $offer, 'products' => $products, 'seller' => $seller), true);

		$pdf_path = Yii::app()->assetManager->getBasePath() . DS . 'offer' . DS . $offer['offer_id'];
		$pdf_file_name = 'offer_' . $offer['offer_id'] . '_' . date('dmYHis') . '.pdf';
		$pdf_file_path = $pdf_path . DS . $pdf_file_name;

		$dir_rs = true;

		if (!is_dir($pdf_path)) {
			$dir_rs = CFileHelper::createDirectory($pdf_path, 0777, true);
		}

		if (!$dir_rs) {
			throw new CException('Unable to create directory.');
		}

		$mpdf = new mPDF('UTF-8', 'A4', 0, '', 15, 15, 22, 16, 9, 9, 'P');
		$mpdf->SetHTMLHeader('<div style="border-bottom: 1px solid #43484d; padding-bottom: 6px; color: #43484d; font-size: 12px; font-weight: bold; text-align: right;">' . Lang::t('pdf.tip.page') . ' {PAGENO} ({nb})' . '</div>');
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdf_file_path, 'F');

		Yii::app()->language = $current_lang;

		if (is_file($pdf_file_path)) {
			$builder = Yii::app()->db->schema->commandBuilder;
			$today = date('Y-m-d H:i:s');

			// insert request
			$update = array(
				'saved' => $today,
				'offer_pdf' => $pdf_file_name,
			);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "offer_id = :offer_id", 
					"params" => array(
						"offer_id" => (int) $offer['offer_id'],
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('offer', $update, $update_criteria)->execute();

				if ($rs) {
					return $pdf_file_name;
				}
			} catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}
}
