<?php
class Faq extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

    public function getGroupsAdmin($offset, $per_page)
    {
        if ($offset) {
            $faqGroups = Yii::app()->db
                ->createCommand("SELECT f.*, fl.faq_title FROM faq_group as f JOIN faq_group_lang as fl ON f.faq_id = fl.faq_id AND fl.language_code = :code". " LIMIT ".$offset.",".$per_page)
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->queryAll();
        } else {
            $faqGroups = Yii::app()->db
                ->createCommand("SELECT f.*, fl.faq_title FROM faq_group as f JOIN faq_group_lang as fl ON f.faq_id = fl.faq_id AND fl.language_code = :code")
                ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
                ->queryAll();
        }


        return $faqGroups;
    }

    public function getQuestionAdmin($offset, $per_page)
    {
        $faqGroups = Yii::app()->db
            ->createCommand("SELECT f.*, fl.quest_title, fl.quest_description, fgl.faq_title FROM faq_question as f JOIN faq_group_lang fgl ON fgl.faq_id = f.group_id and fgl.language_code = :code JOIN faq_question_lang as fl ON f.faq_id = fl.faq_id AND fl.language_code = :code" . " LIMIT ".$offset.",".$per_page)
            ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
            ->queryAll();

        return $faqGroups;
    }

    public function getGroupAdminTotal($per_page = 10)
    {
        $total_groups= Yii::app()->db
            ->createCommand("SELECT COUNT(*) FROM faq_group as f JOIN faq_group_lang as fl ON f.faq_id = fl.faq_id AND fl.language_code = :code")
            ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
            ->queryScalar();

        return array(
            'total' => (int) $total_groups,
            'pages' => ceil($total_groups / $per_page),
        );
    }

    public function getQuestAdminTotal($per_page = 10)
    {
        $total_groups= Yii::app()->db
            ->createCommand("SELECT COUNT(*) FROM faq_question as f JOIN faq_question_lang as fl ON f.faq_id = fl.faq_id AND fl.language_code = :code")
            ->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
            ->queryScalar();

        return array(
            'total' => (int) $total_groups,
            'pages' => ceil($total_groups / $per_page),
        );
    }

    public function getGroupByIdAdmin($id)
    {
        $group = Yii::app()->db
            ->createCommand("SELECT * FROM faq_group WHERE faq_id = :id LIMIT 1")
            ->bindValue(':id', (int) $id, PDO::PARAM_INT)
            ->queryRow();

        if (!empty($group)) {
            // page langs
            $group_langs = Yii::app()->db
                ->createCommand("SELECT * FROM faq_group_lang WHERE faq_id = :id")
                ->bindValue(':id', (int) $id, PDO::PARAM_INT)
                ->queryAll();

            if (!empty($group_langs)) {
                foreach ($group_langs as $grouplang) {
                    $code = $grouplang['language_code'];

                    if (isset(Yii::app()->params->langs[$code])) {
                        $group[$code] = $grouplang;
                    }
                }
            }
        }

        return $group;
    }

    public function getQuestByIdAdmin($id)
    {
        $group = Yii::app()->db
            ->createCommand("SELECT * FROM faq_question WHERE faq_id = :id LIMIT 1")
            ->bindValue(':id', (int) $id, PDO::PARAM_INT)
            ->queryRow();

        if (!empty($group)) {
            // page langs
            $group_langs = Yii::app()->db
                ->createCommand("SELECT * FROM faq_question_lang WHERE faq_id = :id")
                ->bindValue(':id', (int) $id, PDO::PARAM_INT)
                ->queryAll();

            if (!empty($group_langs)) {
                foreach ($group_langs as $grouplang) {
                    $code = $grouplang['language_code'];

                    if (isset(Yii::app()->params->langs[$code])) {
                        $group[$code] = $grouplang;
                    }
                }
            }
        }

        return $group;
    }

    public function add($model, $model_lang)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $insert = [
            'active' => $model['active'],
            'order' => $model['order']
        ];

        $rs = $builder->createInsertCommand('faq_group', $insert)->execute();

        if ($rs) {
            $faqGroupId = (int) Yii::app()->db->getLastInsertID();

            foreach (Yii::app()->params->langs as $language_code => $language_name) {
                // save details
                $insert_lang = array(
                    'faq_id' => $faqGroupId,
                    'language_code' => $language_code,
                );

                foreach ($model_lang->attributes as $field => $value) {
                    $insert_lang[$field] = trim($value[$language_code]);
                }

                $rs = $builder->createInsertCommand('faq_group_lang', $insert_lang)->execute();

                if (!$rs) {
                    $delete_criteria = new CDbCriteria(
                        array(
                            "condition" => "faq_id = :faq_id" ,
                            "params" => array(
                                "faq_id" => $faqGroupId,
                            )
                        )
                    );

                    $builder->createDeleteCommand('faq_group', $delete_criteria)->execute();
                }
            }
        }
    }

    public function addQuestion($model, $model_lang)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $insert = [
            'active' => $model['active'],
            'group_id' => $model['group_id']
        ];

        $rs = $builder->createInsertCommand('faq_question', $insert)->execute();

        if ($rs) {
            $faqQuestId = (int) Yii::app()->db->getLastInsertID();

            foreach (Yii::app()->params->langs as $language_code => $language_name) {
                // save details
                $insert_lang = array(
                    'faq_id' => $faqQuestId,
                    'language_code' => $language_code,
                );

                foreach ($model_lang->attributes as $field => $value) {
                    $insert_lang[$field] = trim($value[$language_code]);
                }

                $rs = $builder->createInsertCommand('faq_question_lang', $insert_lang)->execute();

                if (!$rs) {
                    $delete_criteria = new CDbCriteria(
                        array(
                            "condition" => "faq_id = :faq_id" ,
                            "params" => array(
                                "faq_id" => $faqQuestId,
                            )
                        )
                    );

                    $builder->createDeleteCommand('faq_question_lang', $delete_criteria)->execute();
                }
            }
        }
    }

    public function update($id, $model, $model_lang)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $update_criteria = new CDbCriteria(
            array(
                "condition" => "faq_id = :id" ,
                "params" => array(
                    "id" => (int) $id,
                )
            )
        );

        $update = [
            'active' => $model['active'],
            'order' => $model['order']
        ];

        $rs = $builder->createUpdateCommand('faq_group', $update, $update_criteria)->execute();


        foreach (Yii::app()->params->langs as $language_code => $language_name) {
            // save details
            $update_lang = [];

            $update_criteria = new CDbCriteria(
                array(
                    "condition" => "faq_id = :faq_id AND language_code = :lang" ,
                    "params" => array(
                        "faq_id" => (int) $id,
                        "lang" => $language_code,
                    )
                )
            );

            foreach ($model_lang->attributes as $field => $value) {
                $update_lang[$field] = trim($value[$language_code]);
            }

            $rs = $builder->createUpdateCommand('faq_group_lang', $update_lang, $update_criteria)->execute();
        }
    }

    public function updateQuestion($id, $model, $model_lang)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $update_criteria = new CDbCriteria(
            array(
                "condition" => "faq_id = :id" ,
                "params" => array(
                    "id" => (int) $id,
                )
            )
        );

        $update = [
            'active' => $model['active'],
            'group_id' => $model['group_id']
        ];

        $rs = $builder->createUpdateCommand('faq_question', $update, $update_criteria)->execute();


        foreach (Yii::app()->params->langs as $language_code => $language_name) {
            // save details
            $update_lang = [];

            $update_criteria = new CDbCriteria(
                array(
                    "condition" => "faq_id = :faq_id AND language_code = :lang" ,
                    "params" => array(
                        "faq_id" => (int) $id,
                        "lang" => $language_code,
                    )
                )
            );

            foreach ($model_lang->attributes as $field => $value) {
                $update_lang[$field] = trim($value[$language_code]);
            }

            $rs = $builder->createUpdateCommand('faq_question_lang', $update_lang, $update_criteria)->execute();
        }
    }

    public function deleteQuestion($id)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $delete_criteria = new CDbCriteria(
            array(
                "condition" => "faq_id = :id" ,
                "params" => array(
                    "id" => (int) $id,
                )
            )
        );


        $rs = $builder->createDeleteCommand('faq_question', $delete_criteria)->execute();
        $rs = $builder->createDeleteCommand('faq_question_lang', $delete_criteria)->execute();
    }

    public function delete($id)
    {
        $builder = Yii::app()->db->schema->commandBuilder;

        $delete_criteria = new CDbCriteria(
            array(
                "condition" => "faq_id = :id" ,
                "params" => array(
                    "id" => (int) $id,
                )
            )
        );


        $rs = $builder->createDeleteCommand('faq_group', $delete_criteria)->execute();
        $rs = $builder->createDeleteCommand('faq_group_lang', $delete_criteria)->execute();
    }
}