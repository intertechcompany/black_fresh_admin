<?php

class PhoneRequest extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

    /**
     * @return int
     */
    public function getAdminCountPhoneRequest(): int
    {
        return (int)Yii::app()->db
            ->createCommand("SELECT COUNT(*) FROM phone_request")
            ->queryScalar();
    }

    /**
     * @return int
     */
    public function getCountNew(): int
    {
        return (int)Yii::app()->db
            ->createCommand("SELECT COUNT(*) FROM phone_request where completed = false")
            ->queryScalar();
    }

    public function getAdminPhoneRequest($offset, $perPage)
    {
        return Yii::app()->db
            ->createCommand("SELECT * FROM phone_request ORDER BY created DESC LIMIT {$offset}, {$perPage}")
            ->queryAll();
    }

    public function setApproveStatus($id)
    {
        if (!$id) {
            return;
        }

        $criteria = new CDbCriteria(
            array(
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $id
                ]
            )
        );

        $builder = Yii::app()->db->schema->commandBuilder;
        $builder->createUpdateCommand(
            'phone_request',
            [
                'completed' => true,
                'updated' => date('Y-m-d H:i:s')
            ],
            $criteria
        )->execute();
    }
}