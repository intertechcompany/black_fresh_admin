<?php
class Order extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getNewOrdersTotal()
	{
		$total_orders = Yii::app()->db
			->createCommand("SELECT COUNT(*) FROM `order` WHERE is_new = 1")
			->queryScalar();

		return $total_orders;
	}

	public function getOrdersAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		$search_sql = array();
		$bind_sql = array();

		if (!empty($func_args[1])) {
			$order_id = (int) $func_args[1];
			$email = $func_args[1];
			$keyword_plus = '+' . preg_replace('#\s+#u', ' +', trim($func_args[1]));
			$keyword_quoted = '"' . trim($func_args[1]) . '"';
			$keyword = '(' . $keyword_plus . ') (' . $keyword_quoted . ')';

			$search_sql[] = '(order_id = :id OR MATCH (full_name) AGAINST (:keyword IN BOOLEAN MODE) OR email = :email)';
			
			$bind_sql[] = array(
				'param' => ':id',
				'value' => $order_id,
				'type'  => PDO::PARAM_INT,
			);

			$bind_sql[] = array(
				'param' => ':email',
				'value' => $email,
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':keyword',
				'value' => $keyword,
				'type'  => PDO::PARAM_STR,
			);
		}

		$timezone = new DateTimeZone(Yii::app()->timeZone);

		if (!empty($func_args[2]) && CDateTimeParser::parse($func_args[2], 'dd.MM.yyyy') !== false && !empty($func_args[3]) && CDateTimeParser::parse($func_args[3], 'dd.MM.yyyy') !== false) {
			$search_sql[] = '(created >= :date_from AND created < :date_to)';

			// prepare dates
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[2], $timezone);
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[3], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[2]) && CDateTimeParser::parse($func_args[2], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created >= :date_from';

			// prepare date
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[2], $timezone);

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[3]) && CDateTimeParser::parse($func_args[3], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created < :date_to';

			// prepare date
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[3], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($search_sql)) {
			$total_orders_db = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM `order` WHERE " . implode(' AND ', $search_sql));
				
			foreach ($bind_sql as $bind) {
				$total_orders_db->bindValue($bind['param'], $bind['value'], $bind['type']);
			}
			
			$total_orders = $total_orders_db->queryScalar();
		}
		else {
			$total_orders = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM `order`")
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_orders,
			'pages' => ceil($total_orders / $per_page),
		);
	}

	public function getOrdersAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'order_id':
				$order_by = ($direction == 'asc') ? 'order_id' : 'order_id DESC';
				break;
			case 'created':
				$order_by = ($direction == 'asc') ? 'created' : 'created DESC';
				break;
			default:
				$order_by = 'order_id DESC';
		}

		$func_args = func_get_args();

		$search_sql = array();
		$bind_sql = array();

		if (!empty($func_args[4])) {
			$order_id = (int) $func_args[4];
			$email = $func_args[4];
			$keyword_plus = '+' . preg_replace('#\s+#u', ' +', trim($func_args[4]));
			$keyword_quoted = '"' . trim($func_args[1]) . '"';
			$keyword = '(' . $keyword_plus . ') (' . $keyword_quoted . ')';

			$search_sql[] = '(order_id = :id OR MATCH (full_name) AGAINST (:keyword IN BOOLEAN MODE) OR email = :email)';
			
			$bind_sql[] = array(
				'param' => ':email',
				'value' => $email,
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':id',
				'value' => $order_id,
				'type'  => PDO::PARAM_INT,
			);

			$bind_sql[] = array(
				'param' => ':keyword',
				'value' => $keyword,
				'type'  => PDO::PARAM_STR,
			);
		}

		$timezone = new DateTimeZone(Yii::app()->timeZone);

		if (!empty($func_args[5]) && CDateTimeParser::parse($func_args[5], 'dd.MM.yyyy') !== false && !empty($func_args[6]) && CDateTimeParser::parse($func_args[6], 'dd.MM.yyyy') !== false) {
			$search_sql[] = '(created >= :date_from AND created < :date_to)';

			// prepare dates
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[5], $timezone);
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[6], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[5]) && CDateTimeParser::parse($func_args[5], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created >= :date_from';

			// prepare date
			$date_from = DateTime::createFromFormat('d.m.Y', $func_args[5], $timezone);

			$bind_sql[] = array(
				'param' => ':date_from',
				'value' => $date_from->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}
		elseif (!empty($func_args[6]) && CDateTimeParser::parse($func_args[6], 'dd.MM.yyyy') !== false) {
			$search_sql[] = 'created < :date_to';

			// prepare date
			$date_to = DateTime::createFromFormat('d.m.Y', $func_args[6], $timezone);
			$date_to->add(new DateInterval('P1D'));

			$bind_sql[] = array(
				'param' => ':date_to',
				'value' => $date_to->format('Y-m-d 00:00:00'),
				'type'  => PDO::PARAM_STR,
			);
		}

		if (!empty($search_sql)) {
			$orders_db = Yii::app()->db
				->createCommand("SELECT * FROM `order` WHERE " . implode(' AND ', $search_sql) . " ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page);
			
			foreach ($bind_sql as $bind) {
				$orders_db->bindValue($bind['param'], $bind['value'], $bind['type']);
			}
			
			$orders = $orders_db->queryAll();
		}
		else {
			$orders = Yii::app()->db
				->createCommand("SELECT * FROM `order` ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->queryAll();
		}
			
		return $orders;
	}

	public function getUserOrders($user_id)
	{
		$orders = Yii::app()->db
			->createCommand("SELECT * FROM `order` WHERE user_id = :user_id ORDER BY order_id DESC")
			->bindValue(':user_id', (int) $user_id, PDO::PARAM_INT)
			->queryAll();

		return $orders;
	}

	public function getOrderByIdAdmin($id)
	{
		$order = Yii::app()->db
			->createCommand("SELECT o.*, d.discount_code, d.discount_type, d.discount_value as value 
								FROM `order` as o 
								LEFT JOIN discount as d 
								ON o.discount_id = d.discount_id 
								WHERE o.order_id = :id 
								LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();
			
		return $order;
	}

	public function getOrderProducts($order_id)
	{
		$order_products = Yii::app()->db
			->createCommand("SELECT oi.*, p.product_alias, p.product_sku, p.product_price_type, p.product_pack_size, p.product_photo, p.product_price, pl.product_title 
							 FROM order_item as oi 
							 LEFT JOIN product as p 
							 ON oi.product_id = p.product_id 
							 LEFT JOIN product_lang as pl 
							 ON p.product_id = pl.product_id AND pl.language_code = :code AND pl.product_visible = 1 
							 WHERE oi.order_id = :order_id 
							 ORDER BY oi.order_item_id")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->bindValue(':order_id', (int) $order_id, PDO::PARAM_INT)
			->queryAll();

		if (!empty($order_products)) {
			$variant_ids = array_filter(array_column($order_products, 'variant_id'), function($v) {
    			return $v > 0;
			});

			if (!empty($variant_ids)) {
				$variants = Product::model()->getVariantsByIds($variant_ids);

				foreach ($order_products as $key => $order_product) {
					$order_products[$key]['variant'] = array();
					$variant_id = $order_product['variant_id'];

					if ($variant_id && isset($variants[$variant_id])) {
						$order_products[$key]['variant'] = $variants[$variant_id];
					}
				}
			}

			foreach ($order_products as $key => $order_product) {
                $order_products[$key]['options'] = Yii::app()->db
                    ->createCommand("SELECT * FROM order_item_option WHERE order_item_id = :item_id")
                    ->bindValue(':item_id', (int) $order_product['order_item_id'], PDO::PARAM_INT)
                    ->queryAll();
            }
		}
			
		return $order_products;
	}

	public function save($model)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'order_id',
			'is_new',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array();

		// photos attributes
		$save_images = array();

		if (empty($model->order_id)) {
			// insert order
			$insert_order = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_order[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$insert_order[$field] = $date->format('Y-m-d');
				}
				else {
					$insert_order[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('order', $insert_order)->execute();

				if ($rs) {
					$order_id = (int) Yii::app()->db->getLastInsertID();

					return $order_id;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$order_before = $this->getOrderByIdAdmin($model->order_id);

			$update_order = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_order[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					$date = new DateTime($value);
					$update_order[$field] = $date->format('Y-m-d');
				}
				else {
					$update_order[$field] = $value;
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "order_id = :order_id" , 
					"params" => array(
						"order_id" => $model->order_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('order', $update_order, $update_criteria)->execute();

				if ($rs) {
					if (Yii::app()->params->settings['stock'] != 'none' && $model->status == 'cancelled' && $order_before['status'] != 'cancelled') {
						$this->addOrderQty($model->order_id);
					}
					
					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	public function addOrderQty($order_id)
	{
		$product_model = Product::model();
		$products_to_update = [];
		$products = Yii::app()->db
			->createCommand("SELECT oi.*, p.product_instock, p.product_stock_qty, p.product_price_type, pv.variant_instock, pv.variant_stock_qty 
							FROM order_item as oi 
							LEFT JOIN product as p 
							ON oi.product_id = p.product_id 
							LEFT JOIN product_variant as pv 
							ON oi.variant_id = pv.variant_id 
							WHERE oi.order_id = :order_id 
							ORDER BY oi.order_item_id")
			->bindValue(':order_id', (int) $order_id, PDO::PARAM_INT)
			->queryAll();

		foreach ($products as $product) {
			$product_id = $product['product_id'];
			$variant_id = $product['variant_id'];
			$quantity = $product['quantity'];

			$qty_rs = $product_model->addStockQty($product);

			if ($qty_rs) {
				$products_to_update[$product_id] = !empty($variant_id) ? 'variants' : 'item';
			}
		}

		if (!empty($products_to_update)) {
			$product_model->updateProductsStockStatus($products_to_update);
		}
	}

	public function toggleNew($order_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_order = array(
			'saved' => $today,
			'is_new' => 0,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "order_id = :order_id" , 
				"params" => array(
					"order_id" => $order_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('order', $update_order, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($order_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "order_id = :order_id" , 
				"params" => array(
					"order_id" => $order_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('order', $delete_criteria)->execute();

			if ($rs) {
				$builder->createDeleteCommand('order_item', $delete_criteria)->execute();

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function generateInvoice($order, $products)
	{
		$current_lang = Yii::app()->language;
		Yii::app()->language = 'fi';

		require_once Yii::getPathOfAlias('application.vendor.Mpdf') . DS . 'autoload.php';

		$html = Yii::app()->getController()->renderPartial('//pdf/invoice', array('order' => $order, 'products' => $products), true);

		$pdf_path = Yii::app()->assetManager->getBasePath() . DS . 'order' . DS . $order['order_id'];
		$pdf_file_name = 'invoice_' . $order['order_id'] . '_' . date('dmYHis') . '.pdf';
		$pdf_file_path = $pdf_path . DS . $pdf_file_name;

		$dir_rs = true;

		if (!is_dir($pdf_path)) {
			$dir_rs = CFileHelper::createDirectory($pdf_path, 0777, true);
		}

		if (!$dir_rs) {
			throw new CException('Unable to create directory.');
		}

		$mpdf = new mPDF('UTF-8');
		$mpdf->showImageErrors = true;
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdf_file_path, 'F');

		Yii::app()->language = $current_lang;

		if (is_file($pdf_file_path)) {
			$builder = Yii::app()->db->schema->commandBuilder;
			$today = date('Y-m-d H:i:s');

			// insert request
			$update = array(
				'saved' => $today,
				'invoice_pdf' => $pdf_file_name,
			);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "order_id = :order_id", 
					"params" => array(
						"order_id" => (int) $order['order_id'],
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('order', $update, $update_criteria)->execute();

				if ($rs) {
					return $pdf_file_name;
				}
			} catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}
}
