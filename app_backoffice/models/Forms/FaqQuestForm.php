<?php

/**
 * PageForm class.
 * PageForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class FaqQuestForm extends CFormModel
{
	public $group_id;
	public $active;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'group_id, active',
				'safe',
			),
		);
	}
}