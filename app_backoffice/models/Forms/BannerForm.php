<?php

/**
 * BannerForm class.
 * BannerForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class BannerForm extends CFormModel
{
	public $banner_id;
	public $active;
	public $banner_logo;
	public $banner_url;
	public $banner_url_blank;
	public $banner_position;
	public $banner_place;
	public $banner_type;

	// unnecessary attributes
	public $del_banner_logo;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'banner_id',
				'isValidBanner',
				'on' => 'edit',
			),
			array(
				'banner_id',
				'safe',
				'on' => 'add',
			),
			array(
				'banner_url_blank',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('banners', '\'Open in new tab\' value is invalid!'),
			),
			array(
				'banner_logo',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'banner_place',
				'in',
				'range' => array('slider', 'section_1', 'section_2', 'section_3'),
				'message' => Yii::t('banners', '\'Banner place\' value is invalid!'),
			),
			array(
				'banner_type',
				'in',
				'range' => array('horizontal', 'vertical'),
				'message' => Yii::t('banners', '\'Banner type\' value is invalid!'),
			),
			array(
				'active, banner_url, banner_position,
				del_banner_logo',
				'safe',
			),
		);
	}
	
	public function isValidBanner($attribute, $params)
	{
		$banner = Banner::model()->getBannerByIdAdmin($this->$attribute);

		if (empty($banner)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}