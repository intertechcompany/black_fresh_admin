<?php

/**
 * CourseForm class.
 * CourseForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class CourseForm extends CFormModel
{
	public $course_id;
	public $active;
	public $author_id;
	public $course_alias;
	public $course_position;
	public $course_image;
	public $course_date;
	public $course_price;
	public $course_duration;
	public $course_duration_week;
	public $course_type;
	public $course_content;
	public $authors;

	public $del_course_image;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'course_id',
				'isValidCourse',
				'on' => 'edit',
			),
			array(
				'course_id',
				'safe',
				'on' => 'add',
			),
			array(
				'course_image',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'course_date',
				'filter',
				'filter' => [$this, 'filterDate'],
			),
			array(
				'course_price',
				'filter',
				'filter' => 'floatval',
			),
			array(
				'course_duration',
				'filter',
				'filter' => 'floatval',
			),
			array(
				'course_duration_week',
				'filter',
				'filter' => 'intval',
			),
			array(
				'course_type',
				'in',
				'range' => array('event', 'full'),
				'message' => Yii::t('courses', '\'Type\' value is invalid!'),
			),
			array(
				'authors',
				'isValidAuthors',
			),
			array(
				'active, course_alias, course_position, course_content, 
				del_course_image',
				'safe',
			),
		);
	}
	
	public function isValidCourse($attribute, $params)
	{
		$course = Course::model()->getCourseByIdAdmin($this->$attribute);

		if (empty($course)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function filterDate($date)
	{
		if (preg_match('#\d{2}\.\d{2}\.\d{4} \d{2}:\d{2}#ui', $date)) {
			$d = DateTime::createFromFormat('d.m.Y H:i', $date);
		} else {
			$d = DateTime::createFromFormat('d.m.Y', $date);
		}

		if ($d === false) {
			return '0000-00-00 00:00:00';
		} else {
			return $d->format('Y-m-d H:i:s');
		}
	}
	
	public function isValidAuthors($attribute, $params)
	{
		if (empty($this->$attribute)) {
			return true;
		}

        foreach ($this->$attribute as $value) {
            $author = Author::model()->getAuthorByIdAdmin($value);

            if (empty($author)) {
                $this->addError($attribute, Yii::t('courses', 'Author is invalid!'));

                return false;
            }
        }

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}