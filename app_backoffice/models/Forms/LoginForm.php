<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class LoginForm extends CFormModel
{
	public $mail;
	public $password;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'mail, password',
				'required',
				'message' => Yii::t('login', 'Please enter an email and password! err'),
			),
		);
	}
	
	public function getErrors($attribute = null)
	{
		$json_errors = array();
				
		// get all errors
		foreach(parent::getErrors() as $errors) {
			foreach ($errors as $error) {
				if (!empty($error)) {
					$json_errors[] = $error;
					
					break 2;
				}
			}
		}
		
		return $json_errors;
	}
	
	public function getPageErrors()
	{
		$page_errors = array();
				
		// get all errors
		foreach(parent::getErrors() as $errors) {
			foreach ($errors as $error) {
				if (!empty($error)) {
					$page_errors[] = $error;
					
					break 2;
				}
			}
		}
		
		return $page_errors;
	}
}