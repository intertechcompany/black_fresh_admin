<?php

/**
 * PageForm class.
 * PageForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class FaqGroupForm extends CFormModel
{
	public $order;
	public $active;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'order, active',
				'safe',
			),
		);
	}
}