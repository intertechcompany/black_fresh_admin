<?php

/**
 * PageLangForm class.
 * PageLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class FaqLangQuestForm extends CFormModel
{
	public $quest_title;
	public $quest_description;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'quest_title',
				'requiredMultiLang',
				'message' => Yii::t('page', 'Enter a page title in all languages!'),
			),
            array(
                'quest_description',
                'requiredMultiLang',
                'message' => Yii::t('page', 'Enter a page title in all languages!'),
            ),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		foreach (Yii::app()->params->langs as $code => $lang) {
			if (empty($lang_fields[$code])) {
				$this->addError($attribute, $params['message']);	
				
				break;
			}		
		}
	}
}