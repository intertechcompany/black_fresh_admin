<?php

/**
 * MemberForm class.
 * MemberForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class MemberForm extends CFormModel
{
	public $member_id;
	public $active;
	public $member_position;
	public $member_facebook;
	public $member_image;

	public $del_member_image;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'member_id',
				'isValidMember',
				'on' => 'edit',
			),
			array(
				'member_id',
				'safe',
				'on' => 'add',
			),
			array(
				'member_image',
				'file',
				'allowEmpty' => true,
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => Yii::t('app', 'Image wrong extension type!'),
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => Yii::t('app', 'Maximum file size is {size}!', array('{size}' => '10 MB')),
			),
			array(
				'active, member_position, member_facebook, 
				del_member_image',
				'safe',
			),
		);
	}
	
	public function isValidMember($attribute, $params)
	{
		$member = Member::model()->getMemberByIdAdmin($this->$attribute);

		if (empty($member)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}