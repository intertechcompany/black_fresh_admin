<?php

/**
 * ProductForm class.
 * ProductForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class ProductForm extends CFormModel
{
	public $product_id;
	public $active;
	public $category_id;
	public $brand_id;
	public $collection_id;
	public $product_alias;
	public $product_sku;
	public $product_newest;
	public $product_bestseller;
	public $product_sale;
	public $product_extended;
	public $product_photo;
	public $product_rating;
	public $product_price_type;
	public $product_price;
	public $product_price_old;
	public $product_instock;
	public $product_stock_qty;
	public $product_set;
	public $product_weight;
	public $product_length;
	public $product_width;
	public $product_pack_size;
	public $product_pack_qty;
	public $product_calc_type;
	public $product_video;
	public $product_360_view;
	public $product_vr_view;
	public $categories;
	public $is_replacement;
	public $replacements;
	public $related;
    public $is_for_parents;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'product_id',
				'isValidProduct',
				'on' => 'edit',
			),
			array(
				'product_id',
				'safe',
				'on' => 'add',
			),
			array(
				'brand_id',
				'isValidBrand',
			),
			array(
				'collection_id',
				'isValidCollection',
			),
			/* array(
				'product_price',
				'required',
				'message' => Yii::t('products', 'Enter a price!'),
			), */
			array(
				'category_id',
				'isValidCategory',
				'skipOnError' => true,
			),
			array(
				'categories',
				'isValidCategories',
			),
			array(
				'product_newest',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('products', '\'Newest\' value is invalid!'),
			),
			array(
				'product_bestseller',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('products', '\'Best seller\' value is invalid!'),
			),
			array(
				'product_sale',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('products', '\'Sale\' value is invalid!'),
			),
			array(
				'product_extended',
				'in',
				'range' => array(0, 1),
				'message' => Yii::t('products', '\'Extended\' value is invalid!'),
			),
			array(
				'product_price_type',
				'in',
				'range' => array('piece', 'package', 'per_meter', 'item', 'variants', 'set'),
				'message' => Yii::t('products', '\'Product price type\' value is invalid!'),
			),
			array(
				'product_calc_type',
				'in',
				'range' => array('', 'floor', 'wall'),
				'message' => Yii::t('products', '\'Product calc type\' value is invalid!'),
			),
			array(
				'product_instock',
				'in',
				'range' => array('in_stock', 'out_of_stock', 'preorder'),
				'message' => Yii::t('products', '\'In stock\' value is invalid!'),
			),
			array(
				'product_price, product_price_old, product_pack_size',
				'filter',
				'filter' => 'floatval',
			),
			array(
				'product_length, product_width, product_pack_qty',
				'filter',
				'filter' => 'intval',
			),
			array(
				'product_weight',
				'filter',
				'filter' => 'floatval',
			),
			array(
				'active, replacements, related, product_alias, is_replacement, product_sku, product_price_old, product_set, product_stock_qty, product_photo, product_rating, product_video, product_360_view, product_vr_view',
				'safe',
			),
            array(
                'is_for_parents',
                'in',
                'range' => array(0, 1),
                'message' => Yii::t('products', '\'For parents\' value is invalid!'),
            ),
		);
	}
	
	public function isValidProduct($attribute, $params)
	{
		$product = Product::model()->getProductByIdAdmin($this->$attribute);

		if (empty($product)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function isValidCategory($attribute, $params)
	{
		$category = Category::model()->getCategoryByIdAdmin($this->$attribute);

		if (empty($category)) {
			$this->addError($attribute, Yii::t('products', 'Category is invalid!'));

			return false;
		}

		return true;
	}

	public function isValidCategories($attribute, $params)
	{
		if (empty($this->$attribute)) {
			return true;
		}

        foreach ($this->$attribute as $value) {
            $category = Category::model()->getCategoryByIdAdmin($value);

            if (empty($category)) {
                $this->addError($attribute, Yii::t('products', 'Category is invalid!'));

                return false;
            }
        }

		return true;
	}

	public function isValidBrand($attribute, $params)
	{
		if (empty($this->$attribute)) {
			return true;
		}

		$brand = Brand::model()->getBrandByIdAdmin($this->$attribute);

		if (empty($brand)) {
			$this->addError($attribute, Yii::t('products', 'Brand is invalid!'));

			return false;
		}

		return true;
	}

	public function isValidCollection($attribute, $params)
	{
		if (empty($this->$attribute)) {
			return true;
		}

		$collection = Collection::model()->getCollectionByIdAdmin($this->$attribute);

		if (empty($collection)) {
			$this->addError($attribute, Yii::t('products', 'Collection is invalid!'));

			return false;
		}

		return true;
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}
