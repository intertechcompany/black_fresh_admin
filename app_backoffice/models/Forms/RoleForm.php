<?php

/**
 * RoleForm class.
 * RoleForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class RoleForm extends CFormModel
{
	public $role_id;
	public $role_name;
	public $role_rights;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'role_id',
				'isValidRole',
				'on' => 'edit',
			),
			array(
				'role_id',
				'safe',
				'on' => 'add',
			),
			array(
				'role_name',
				'required',
				'message' => Yii::t('roles', 'Enter a manager login!'),
			),
			array(
				'role_rights',
				'filter',
				'filter' => 'json_encode',
			),
		);
	}
	
	public function isValidRole($attribute, $params)
	{
		$product = Role::model()->getRoleByIdAdmin($this->$attribute);

		if (empty($product)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}

	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}