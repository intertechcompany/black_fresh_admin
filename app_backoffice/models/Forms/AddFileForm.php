<?php

/**
 * AddFileForm class.
 * AddFileForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class AddFileForm extends CFormModel
{
	public $photo;
	public $video_mp4;
	public $video_webm;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'photo',
				'file',
				'types' => 'jpg, jpeg, gif, png',
				'wrongType' => 'Изображение должно быть в формате JPG, GIF или PNG!',
				'maxSize' => 10 * 1024 * 1024, // 10 MB
				'tooLarge' => 'Размер изображения не должен превышать 10 MB!',
				'message' => 'Выберите файл!',
				'on' => 'photo',
			),
			array(
				'video_mp4',
				'file',
				'allowEmpty' => true,
				'types' => 'mp4',
				'wrongType' => 'Неверный формат видео!',
				'maxSize' => 30 * 1024 * 1024, // 30 MB
				'tooLarge' => 'Размер видео не должен превышать 30 MB!',
				'on' => 'video',
			),
			array(
				'video_webm',
				'file',
				'allowEmpty' => true,
				'types' => 'webm',
				'wrongType' => 'Неверный формат видео!',
				'maxSize' => 30 * 1024 * 1024, // 30 MB
				'tooLarge' => 'Размер видео не должен превышать 30 MB!',
				'on' => 'video',
			),
		);
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
				
		// get all errors
		/*
		foreach($this->getErrors() as $errors) {
			foreach ($errors as $error) {
				if (!empty($error)) {
					$json_errors['msg'][] = $error;
					
					break 2;
				}
			}
		}
		*/
		
		return $json_errors;
	}
}