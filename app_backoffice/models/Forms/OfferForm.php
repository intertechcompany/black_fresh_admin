<?php

/**
 * OfferForm class.
 * OfferForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class OfferForm extends CFormModel
{
	public $offer_id;
	public $first_name;
	public $last_name;
	public $full_name;
	public $email;
	public $phone;
	public $zip;
	public $city;
	public $address;
	public $address_2;
	public $delivery_first_name;
	public $delivery_last_name;
	public $delivery_full_name;
	public $delivery_email;
	public $delivery_phone;
	public $delivery_zip;
	public $delivery_city;
	public $delivery_address;
	public $delivery_address_2;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'offer_id',
				'isValidOffer',
				'on' => 'edit',
			),
			array(
				'offer_id',
				'safe',
				'on' => 'add',
			),
			array(
				'first_name, last_name, email, phone, zip, city, address, address_2, delivery_first_name, delivery_last_name, delivery_email, delivery_phone, delivery_zip, delivery_city, delivery_address, delivery_address_2',
				'safe',
			),
			array(
				'full_name',
				'filter',
				'filter' => array($this, 'fullName'),
			),
			array(
				'delivery_full_name',
				'filter',
				'filter' => array($this, 'deliveryFullName'),
			),
		);
	}
	
	public function isValidOffer($attribute, $params)
	{
		$product = Offer::model()->getOfferByIdAdmin($this->$attribute);

		if (empty($product)) {
			$this->addError($attribute, Yii::t('app', 'Invalid data! Try to repeat the action after page refresh'));

			return false;
		}

		return true;
	}
	
	public function fullName($full_name)
	{
		$full_name = trim($this->first_name . ' ' . $this->last_name);

		return $full_name;
	}

	public function deliveryFullName($full_name)
	{
		$full_name = trim($this->delivery_first_name . ' ' . $this->delivery_last_name);

		return $full_name;
	}

	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}