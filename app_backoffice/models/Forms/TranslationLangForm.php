<?php

/**
 * TranslationLangForm class.
 * TranslationLangForm is the data structure for keeping
 * write mail form data. It is used by the 'review' action of 'AjaxController'.
 */
class TranslationLangForm extends CFormModel
{
	public $translation_value;

	private $_myErrors = array();
	private $_errorFields = array();

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array(
				'translation_value',
				'requiredMultiLang',
				'message' => Yii::t('translation', 'Enter a translation value in base language ({lang})!', array('{lang}' => Yii::app()->params->lang)),
			),
		);
	}

	public function requiredMultiLang($attribute, $params)
	{
		$lang_fields = $this->$attribute;

		// default language is English
		if (empty($lang_fields[Yii::app()->params->lang])) {
			$this->addError($attribute, $params['message']);
		}
	}
	
	public function afterValidate()
	{
		foreach ($this->attributes as $attribute => $value) {
			if ($this->hasErrors($attribute)) {
				$this->_errorFields[] = $attribute;
				
				foreach ($this->getErrors($attribute) as $error) {
					$this->_myErrors[] = $error;
				}
			}
		}
		
		return parent::afterValidate();
	}
	
	public function jsonErrors()
	{
		$json_errors = array(
			'msg' => array_unique($this->_myErrors),
			'fields' => array_unique($this->_errorFields),
		);
		
		return $json_errors;
	}
}