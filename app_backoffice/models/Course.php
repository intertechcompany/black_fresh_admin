<?php
class Course extends CModel
{
	private $imagine;
	private $mode_inset;
	private $mode_outbound;
	
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getCoursesAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$course_id = (int) $func_args[1];
			$course_name = addcslashes($func_args[1], '%_');

			$total_courses = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM course as c JOIN course_lang as cl ON c.course_id = cl.course_id AND cl.language_code = :code WHERE c.course_id = :id OR cl.course_name LIKE :course_name")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $course_id, PDO::PARAM_INT)
				->bindValue(':course_name', '%' . $course_name . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_courses = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM course as c JOIN course_lang as cl ON c.course_id = cl.course_id AND cl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_courses,
			'pages' => ceil($total_courses / $per_page),
		);
	}

	public function getCoursesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'course_id':
				$order_by = ($direction == 'asc') ? 'c.course_id' : 'c.course_id DESC';
				break;
			case 'course_name':
				$order_by = ($direction == 'asc') ? 'cl.course_name' : 'cl.course_name DESC';
				break;
			case 'course_position':
				$order_by = ($direction == 'asc') ? 'c.course_position' : 'c.course_position DESC';
				break;
			default:
				$order_by = 'c.course_position, c.course_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$course_id = (int) $func_args[4];
			$course_name = addcslashes($func_args[4], '%_');

			$courses = Yii::app()->db
				->createCommand("SELECT c.*, cl.course_name FROM course as c JOIN course_lang as cl ON c.course_id = cl.course_id AND cl.language_code = :code WHERE c.course_id = :id OR cl.course_name LIKE :course_name ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $course_id, PDO::PARAM_INT)
				->bindValue(':course_name', '%' . $course_name . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$courses = Yii::app()->db
				->createCommand("SELECT c.*, cl.course_name FROM course as c JOIN course_lang as cl ON c.course_id = cl.course_id AND cl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $courses;
	}

	public function getCourseByIdAdmin($id)
	{
		$course = Yii::app()->db
			->createCommand("SELECT * FROM course WHERE course_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($course)) {
			// course langs
			$course_langs = Yii::app()->db
				->createCommand("SELECT * FROM course_lang WHERE course_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($course_langs)) {
				foreach ($course_langs as $course_lang) {
					$code = $course_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$course[$code] = $course_lang;
					}
				}
			}
			
			// course categories
			/* $course['categories'] = Yii::app()->db
				->createCommand("SELECT category_id FROM category_course WHERE course_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryColumn(); */

			// course authors
			$course['authors'] = Yii::app()->db
				->createCommand("SELECT author_id FROM author_course WHERE course_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryColumn();
		}
			
		return $course;
	}

	public function getCoursesListAdmin()
	{
		$courses = Yii::app()->db
			->createCommand("SELECT c.course_id, cl.course_name FROM course as c JOIN course_lang as cl ON c.course_id = cl.course_id AND cl.language_code = :code ORDER BY cl.course_name")
			->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
			->queryAll();
			
		return $courses;
	}

    public function issetCourseByAlias($course_id, $course_alias)
	{
		if (!empty($course_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM course WHERE course_id != :course_id AND course_alias LIKE :alias")
				->bindValue(':course_id', (int) $course_id, PDO::PARAM_INT)
				->bindValue(':alias', $course_alias, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM course WHERE course_alias LIKE :alias")
				->bindValue(':alias', $course_alias, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function save($model, $model_lang)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'course_id',
			'course_image',
			// 'categories',
			'authors',
			'course_content',
		);

		// integer attributes
		$int_attributes = array(
			'course_position',
		);

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_course_image',
		);

		// photos attributes
		$save_images = array(
			'course_image',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get not empty title
		foreach (Yii::app()->params->langs as $language_code => $language_name) {
			if (!empty($model_lang->course_name[$language_code])) {
				$course_name = $model_lang->course_name[$language_code];
				break;
			}
		}
		
		// get alias
		$model->course_alias = empty($model->course_alias) ? URLify::filter($course_name, 200) : URLify::filter($model->course_alias, 200);

		while ($this->issetCourseByAlias($model->course_id, $model->course_alias)) {
			$model->course_alias = $model->course_alias . '-' . uniqid();
		}

		// get max course position
		if (empty($model->course_position)) {
			$max_position = Yii::app()->db
				->createCommand("SELECT MAX(course_position) FROM course")
				->queryScalar();

			$model->course_position = $max_position + 1;
		}

		if (empty($model->course_id)) {
			// insert course
			$insert_course = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_course[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$insert_course[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$insert_course[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$insert_course[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('course', $insert_course)->execute();

				if ($rs) {
					$model->course_id = (int) Yii::app()->db->getLastInsertID();

					$skip_attributes = array(
						'course_content',
					);

					$int_attributes = array(
						'course_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_course_lang = array(
							'course_id' => $model->course_id,
							'language_code' => $language_code,
							'course_visible' => !empty($model_lang->course_name[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (in_array($field, $skip_attributes) || !is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_course_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_course_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('course_lang', $insert_course_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "course_id = :course_id" , 
									"params" => array(
										"course_id" => $model->course_id,
									)
								)
							);
							
							$builder->createDeleteCommand('course', $delete_criteria)->execute();

							return false;
						}
					}

					$this->saveInfoblocks($model, $model_lang);
					// $this->saveCategories($model, $model->categories);
					$this->saveAuthors($model, $model->authors);
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_course = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, array_merge($skip_attributes, $del_attributes))) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_course[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
						$update_course[$field] = '0000-00-00';
					}
					else {
						$date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
						$update_course[$field] = $date->format('Y-m-d');
					}
				}
				else {
					$update_course[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_course[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "course_id = :course_id" , 
					"params" => array(
						"course_id" => $model->course_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('course', $update_course, $update_criteria)->execute();

				if ($rs) {
					// delete photos
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'course' . DS . $model->course_id . DS . $model->$del_attribute;

							if (is_file($photo_path)) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}
						}
					}

					$skip_attributes = array(
						'course_content',
					);

					$int_attributes = array(
						'course_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_course_lang = array(
							'course_visible' => !empty($model_lang->course_name[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'course_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (in_array($field, $skip_attributes) || !is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_course_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_course_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "course_id = :course_id AND language_code = :lang" , 
								"params" => array(
									"course_id" => (int) $model->course_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('course_lang', $update_course_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					$this->saveInfoblocks($model, $model_lang);
					// $this->saveCategories($model, $model->categories);
					$this->saveAuthors($model, $model->authors);
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function saveInfoblocks($model, $model_lang)
	{
		$course_content = array();
		$course_content_lang = array();

		// define fields set
		$fields_list = array(
			'course_content' => array(
				'file_multiple' => array(
					// 'photo',
				),
				'file' => array(
					// 'bg_photo',
				),
				'text' => array(
					// 'type',
				),
				'text_lang' => array(
					'title',
					'text',
				),
			),
		);

		// base lang
		$langs = Yii::app()->params->langs;
		$lang_code = Yii::app()->params->lang;

		foreach ($fields_list as $field => $fields) {
			if (!empty($model->$field)) {
				$position = 0;

				foreach ($model->$field as $index => $infoblock) {
					$field_data = array();	
					$field_lang_data = array();	

					foreach ($fields['file_multiple'] as $file_field) {
						$info_files = array();

						if (!empty($infoblock['del_' . $file_field])) {
							foreach ($infoblock['del_' . $file_field] as $i => $del_file) {
								$photo_path = Yii::app()->assetManager->basePath . DS . 'course' . DS . $model->course_id . DS . $del_file;

								if (is_file($photo_path) && is_dir(dirname($photo_path))) {
									CFileHelper::removeDirectory(dirname($photo_path));
								}

								if (!empty($infoblock['current_' . $file_field][$i])) {
									unset($infoblock['current_' . $file_field][$i]);
								}	
							}
						}

						if (!empty($infoblock['current_' . $file_field])) {
							$info_files = $infoblock['current_' . $file_field];
						}

						$file_type = ($file_field == 'video') ? 'video_mp4' : 'photo';
						
						$photos = CUploadedFile::getInstancesByName($field . '[' . $index . '][' . $file_field . ']');

						if (!empty($photos)) {
							foreach ($photos as $photo) {
								$file_model = new AddFileForm($file_type);
								$file_model->$file_type = $photo;

								if (!empty($file_model->$file_type)) {
									if ($file_model->validate()) {
										$info_files[] = $this->saveFile($model, $file_model->$file_type, $file_type);
									}
								}
							}
						}

						$field_data[$file_field] = empty($info_files) ? '' : $info_files;
					}

					foreach ($fields['file'] as $file_field) {
						$info_file = '';

						if (!empty($infoblock['del_' . $file_field])) {
							$photo_path = Yii::app()->assetManager->basePath . DS . 'course' . DS . $model->course_id . DS . $infoblock['del_' . $file_field];

							if (is_file($photo_path) && is_dir(dirname($photo_path))) {
								CFileHelper::removeDirectory(dirname($photo_path));
							}

							if (!empty($infoblock['current_' . $file_field])) {
								unset($infoblock['current_' . $file_field]);
							}
						}

						if (!empty($infoblock['current_' . $file_field])) {
							$info_file = $infoblock['current_' . $file_field];
						}

						$file_type = ($file_field == 'video') ? 'video_mp4' : 'photo';

						$file_model = new AddFileForm($file_type);
						$file_model->$file_type = CUploadedFile::getInstanceByName($field . '[' . $index . '][' . $file_field . ']');

						if (!empty($file_model->$file_type)) {
							if ($file_model->validate()) {
								$info_file = $this->saveFile($model, $file_model->$file_type, $field, $file_type);
							} else {
								$info_file = '';
							}
						}

						$field_data[$file_field] = $info_file;
					}

					// special field position
					$infoblock['position'] = $position;
					$field_data['position'] = $infoblock['position'];

					foreach ($fields['text'] as $text_field) {
						$infoblock[$text_field] = trim($infoblock[$text_field]);
						$field_data[$text_field] = $infoblock[$text_field];
					}

                    foreach (Yii::app()->params->langs as $code => $lang) {
                        foreach ($model_lang->$field[$code] as $lang_index => $lang_infoblock) {
                            foreach ($fields['text_lang'] as $text_lang_field) {
								$lang_infoblock[$code][$text_lang_field] = trim($lang_infoblock[$text_lang_field]);
                            }
						}
                    }
					
					foreach ($fields['text_lang'] as $text_lang_field) {
						foreach (Yii::app()->params->langs as $code => $lang) {
							$field_lang_data[$code][$text_lang_field] = trim($model_lang->$field[$code][$index][$text_lang_field]);
							
							if ($code != $lang_code && empty($field_lang_data[$code][$text_lang_field])) {
								// set base lang field
								$field_lang_data[$code][$text_lang_field] = $field_lang_data[$lang_code][$text_lang_field];
							}
						}
					}

					// if (!empty($infoblock['title' . '_' . $lang_code]) || !empty($infoblock['text' . '_' . $lang_code]) || !empty($field_data['bg_photo']) || !empty($field_data['bg_video']) || !empty($field_data['vimeo'])) {
					if (!empty($field_lang_data[$lang_code]['title']) || !empty($field_lang_data[$lang_code]['text'])) {
						${$field}[$position] = $field_data;

                        foreach (Yii::app()->params->langs as $code => $lang) {
                            ${$field . '_lang'}[$code][$position] = $field_lang_data[$code];
                        }

						$position++;
					}
				}
			}
		}

		$saved = false;
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_course = array(
			'saved' => $today,
			'course_content' => !empty($course_content) ? json_encode($course_content) : '',
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "course_id = :course_id", 
				"params" => array(
					"course_id" => $model->course_id,
				)
			)
		);

		try {
			$builder->createUpdateCommand('course', $update_course, $update_criteria)->execute();

			foreach (Yii::app()->params->langs as $language_code => $lang) {				
				$update_course_lang = array(
					'saved' => $today,
					'course_content' => !empty($course_content_lang[$language_code]) ? json_encode($course_content_lang[$language_code]) : '',
				);
				
				$update_lang_criteria = new CDbCriteria(
					array(
						"condition" => "course_id = :course_id AND language_code = :lang" , 
						"params" => array(
							"course_id" => (int) $model->course_id,
							"lang" => $language_code,
						)
					)
				);

				$builder->createUpdateCommand('course_lang', $update_course_lang, $update_lang_criteria)->execute();
			}

			$saved = true;
		} catch (CDbException $e) {
			// ...
		}

		return $saved;
	}

	private function saveFile($model, $file, $field, $file_type = 'photo')
	{
		if (empty($file)) {
			return false;
		}

		if ($this->imagine === null) {
			$this->initImagine();
		}

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_file = '';

		$file_id = uniqid();
		$file_name = strtolower(str_replace('-', '_', URLify::filter($file->getName(), 60, 'ru', true)));
		
		$file_dir = Yii::app()->assetManager->basePath . DS . 'page' . DS . $model->page_id . DS . $file_id . DS;
		$file_path = $file_dir . 'tmp_' . $file_name;

		$dir_rs = true;

		if (!is_dir($file_dir)) {
			$dir_rs = CFileHelper::createDirectory($file_dir, 0777, true);
		}

		if ($dir_rs) {
			$save_rs = $file->saveAs($file_path);

			if ($save_rs) {
				$file_name_parts = explode('.', $file_name);
				$file_ext = array_pop($file_name_parts);

				$file = $file_dir . $file_name;
				$file_save_name = $file_id . '/' . $file_name;

				copy($file_path, $file);

				if ($file_type == 'photo') {
					$image_obj = $this->imagine->open($file_path);
					$original_image_size = $image_obj->getSize();

					$save_file = array(
						'file' => $file_save_name,
						'w' => $original_image_size->getWidth(),
						'h' => $original_image_size->getHeight(),
					);
				} else {
					$save_file = array(
						'file' => $file_save_name,
					);
				}
				
				if (!is_file($file)) {
					$save_file = '';
				}

				// remove original file
				unlink($file_path);
			}
		}

		return $save_file;
	}

	private function initImagine()
	{
		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_file_rs = '';

		if (extension_loaded('imagick')) {
			$this->imagine = new Imagine\Imagick\Imagine();
		} elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$this->imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$this->mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$this->mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;
	}

	private function saveCategories($model, $categories)
	{
		$rs_inserted = 0;
		$model->course_id = (int) $model->course_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete categories
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "course_id = :course_id" , 
				"params" => array(
					"course_id" => $model->course_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('category_course', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($categories)) {
			$insert_categories = array();

			foreach ($categories as $category) {
				$category = (int) $category;

				if (!empty($category)) {
					$insert_categories[] = array(
						'course_id' => $model->course_id,
						'category_id' => $category,
					);
				}
			}

			if (!empty($insert_categories)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('category_course', $insert_categories)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}
	
	private function saveAuthors($model, $authors)
	{
		$rs_inserted = 0;
		$model->course_id = (int) $model->course_id;
		$builder = Yii::app()->db->schema->commandBuilder;

		// delete authors
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "course_id = :course_id" , 
				"params" => array(
					"course_id" => $model->course_id,
				)
			)
		);
		
		try {
			$builder->createDeleteCommand('author_course', $delete_criteria)->execute();
		}
		catch (CDbException $e) {
			// ...
		}

		if (!empty($authors)) {
			$insert_authors = array();

			foreach ($authors as $author) {
				$author = (int) $author;

				if (!empty($author)) {
					$insert_authors[] = array(
						'course_id' => $model->course_id,
						'author_id' => $author,
					);
				}
			}

			if (!empty($insert_authors)) {
				try {
					$rs_inserted = $builder->createMultipleInsertCommand('author_course', $insert_authors)->execute();
				}
				catch (CDbException $e) {
					return false;
				}
			}
		}

		return $rs_inserted;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'course' . DS . $model->course_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;

						if ($attribute == 'course_image') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;

							$image_file_details = $image_dir . $image_name . '_d.' . $image_extension; // details
							$image_save_name_details = $image_id . '/' . $image_name . '_d.' . $image_extension;
							$image_file_details_2x = $image_dir . $image_name . '_d@2x.' . $image_extension; // details 2x
							$image_save_name_details_2x = $image_id . '/' . $image_name . '_d@2x.' . $image_extension;

							$image_file_list = $image_dir . $image_name . '_c.' . $image_extension; // list
							$image_save_name_list = $image_id . '/' . $image_name . '_c.' . $image_extension;
							$image_file_list_2x = $image_dir . $image_name . '_c@2x.' . $image_extension; // list 2x
							$image_save_name_list_2x = $image_id . '/' . $image_name . '_c@2x.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							if ($original_image_size->getWidth() < 768 || $original_image_size->getHeight() < 436) {
								continue;
							}

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// details image
							$image_obj = $imagine->open($image_path);
							$resized_image_2x = null;

							if ($original_image_size->getWidth() >= 1536 && $original_image_size->getHeight() >= 872) {
								$resized_image_2x = $image_obj->thumbnail(new Imagine\Image\Box(1536, 872), $mode_outbound)
									->save($image_file_details_2x, array('quality' => 80));
							}
							
							if ($original_image_size->getWidth() >= 768 && $original_image_size->getHeight() >= 436) {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(768, 436), $mode_outbound)
									->save($image_file_details, array('quality' => 80));
								/* $resized_image = $image_obj->resize($original_image_size->widen(768))
									->save($image_file_details, array('quality' => 80)); */
							} else {
								$resized_image = $image_obj->save($image_file_details, array('quality' => 80));
							}

							$image_files['details'] = [
								'1x' => array(
									'path' => $image_save_name_details,
									'size' => array(
										'w' => $resized_image->getSize()->getWidth(),
										'h' => $resized_image->getSize()->getHeight(),
									),
								),
								'2x' => empty($resized_image_2x) ? null : array(
									'path' => $image_save_name_details_2x,
									'size' => array(
										'w' => $resized_image_2x->getSize()->getWidth(),
										'h' => $resized_image_2x->getSize()->getHeight(),
									),
								),
							];

							// list image
							$image_obj = $imagine->open($image_path);
							$resized_image_2x = null;

							if ($original_image_size->getWidth() >= 728 && $original_image_size->getHeight() > 500) {
								$resized_image_2x = $image_obj->thumbnail(new Imagine\Image\Box(728, 500), $mode_outbound)
									->save($image_file_list_2x, array('quality' => 80));
							}

							$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(364, 250), $mode_outbound)
								->save($image_file_list, array('quality' => 80));

							$image_files['list'] = [
								'1x' => array(
									'path' => $image_save_name_list,
									'size' => array(
										'w' => $resized_image->getSize()->getWidth(),
										'h' => $resized_image->getSize()->getHeight(),
									),
								),
								'2x' => empty($resized_image_2x) ? null : array(
									'path' => $image_save_name_list_2x,
									'size' => array(
										'w' => $resized_image_2x->getSize()->getWidth(),
										'h' => $resized_image_2x->getSize()->getHeight(),
									),
								),
							];

							if (is_file($image_file_original) && is_file($image_file_details) && is_file($image_file_list)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						} else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							switch ($attribute) {
								case 'course_photo':
									if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
										$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));	
									}
									break;
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_course = array(
				'saved' => $today,
			);

			$update_course = array_merge($update_course, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "course_id = :course_id" , 
					"params" => array(
						"course_id" => $model->course_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('course', $update_course, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function setPosition($course_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_course = array(
			'saved' => $today,
			'course_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "course_id = :course_id" , 
				"params" => array(
					"course_id" => $course_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('course', $update_course, $update_criteria)->execute();
			
			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function toggle($course_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_course = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "course_id = :course_id" , 
				"params" => array(
					"course_id" => $course_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('course', $update_course, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($course_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$course = $this->getCourseByIdAdmin($course_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "course_id = :course_id" , 
				"params" => array(
					"course_id" => $course_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('course', $delete_criteria)->execute();

			if ($rs) {
				// remove course directory
				if (is_dir($assetPath . DS . 'course' . DS . $course_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'course' . DS . $course_id);
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}