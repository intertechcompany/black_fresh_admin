<?php
class Subscription extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }


    public function getAll($offset = 0, $perPage = 10)
    {
        $currentDate = new DateTime();
        $currentDate->modify('-4 day');
        $date = $currentDate->format("Y-m-d");

        return Yii::app()->db
            ->createCommand("SELECT snp.id, snp.status as snp_status, snp.first_pay, snp.payment as snp_payment, snp.next_date, snp.created as snp_created, snp.updated as snp_updated, snp.delivery_status,
                                so.order_reference, so.price, so.delivery, so.delivery_address, so.np_city, so.np_department, so.created as so_created, so.payment_status,
                                u.user_first_name, u.user_last_name, so.phone,
                                sp.quantity, sp.price, sp.variant_title, sp.option_title, sp.title, sp.milling
                                FROM sub_next_payments as snp 
                                left join sub_order as so
                                on so.order_reference = snp.order_reference
                                left join user as u
                                on u.user_id = so.user_id
                                left join sub_product as sp
                                on so.id = sp.sub_id where snp.next_date >= '$date' order by snp.next_date limit $offset,$perPage")
            ->queryAll();
    }

    public function getCount()
    {
        $currentDate = new DateTime();
        $currentDate->modify('-4 day');
        $date = $currentDate->format("Y-m-d");

        return Yii::app()->db
            ->createCommand("select count(*) as total from sub_next_payments where next_date >= '$date'")
            ->queryRow();
    }

    public function getSubOrderCount()
    {
        return Yii::app()->db
            ->createCommand("select count(*) as total from sub_order")
            ->queryRow();
    }

    public function getExportData($offset, $perIter)
    {
        $subOrders = Yii::app()->db
            ->createCommand("
                SELECT *
                FROM sub_order as s
                LIMIT " . $offset . "," . $perIter
            )
            ->queryAll();

        if (!empty($subOrders)) {
            foreach ($subOrders as $key => $order) {
                $subProduct = Yii::app()->db
                    ->createCommand("select sp.*, p.* from sub_product as sp
                                left join product as p
                                on p.product_id = sp.product_id
                                left join product_variant as pv 
                                ON sp.variant_id = pv.variant_id
                                where sp.sub_id = :subId")
                    ->bindValue(':subId', (int) $order['id'], PDO::PARAM_INT)
                    ->queryRow();

                $subOrders[$key]['product'] = $subProduct;
                $subOrders[$key]['user'] = User::model()->getUserByIdAdmin($order['user_id']);
            }
        }

        return $subOrders;
    }

    public function update($subId)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $subId,
                ]
            ]
        );

        $update = [
            'delivery_status' => 'checked'
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $builder->createUpdateCommand('sub_next_payments', $update, $criteria)->execute();
    }

    public function updateMark($id)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $id,
                ]
            ]
        );

        $update = [
            'checked' => 1
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $builder->createUpdateCommand('sub_user_mark', $update, $criteria)->execute();
    }

    public function getUsersMark()
    {
        return Yii::app()->db
            ->createCommand("select * from sub_user_mark")
            ->queryAll();
    }

    public function getSubByOrder($ref)
    {
        return Yii::app()->db
            ->createCommand("select * from sub_next_payments where order_reference = '$ref'")
            ->queryRow();
    }

    public function getSubAnswer($id)
    {
        return Yii::app()->db
            ->createCommand("SELECT * FROM sub_mark_questions WHERE id = :id LIMIT 1")
            ->bindValue(':id', $id)
            ->queryRow();
    }

    public function getCountNew()
    {
        $currentDay = date('Y-m-d');
        $nextDay = date('Y-m-d', strtotime("+1 day"));

        return Yii::app()->db
            ->createCommand("select count(*) as total from sub_next_payments where next_date > '$currentDay' and next_date <= '$nextDay' and delivery_status = 'new'")
            ->queryRow();
    }
}