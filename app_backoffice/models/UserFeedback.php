<?php


class UserFeedback extends CModel
{
    public function rules()
    {
        return array();
    }

    public function attributeNames()
    {
        return array();
    }

    public static function model()
    {
        return new self();
    }

    public function getUserFeedback($offset = 0, $perPage = 10)
    {
        return Yii::app()->db
            ->createCommand("select f.*, r.reason_uk as reason, u.user_first_name as user_first, u.user_last_name as user_last from user_feedback as f
                            left JOIN user_feedback_reason as r on r.id = f.reason_id LEFT join user as u on u.user_id = f.user_id ORDER BY f.id DESC limit $offset, $perPage")
            ->queryAll();
    }

    public function getUserFiles($feedbackId)
    {
        return Yii::app()->db
            ->createCommand("select f.* from user_feedback_file as f where f.feedback_id in ('$feedbackId')")
            ->queryAll();
    }

    public function update($id)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $id,
                ]
            ]
        );

        $update = [
            'checked' => 1
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $builder->createUpdateCommand('user_feedback', $update, $criteria)->execute();
    }

    public function getCountNew()
    {
        return Yii::app()->db
            ->createCommand("select count(*) as total from user_feedback where checked  = 0")
            ->queryRow();
    }
}