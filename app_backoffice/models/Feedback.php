<?php
class Feedback extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}

	public function getAll($offset = 0, $perPage = 10)
	{
        return Yii::app()->db
            ->createCommand("select f.*, p.page_alias as page, pl.page_title as page_title from feedback as f left JOIN page as p on f.page_id = p.page_id left join page_lang as pl
                            on pl.page_id = p.page_id where pl.language_code = 'ru' ORDER BY f.id DESC limit $offset,$perPage")
            ->queryAll();
	}

	public function getCount()
    {
        return Yii::app()->db
            ->createCommand("select count(*) as total from feedback")
            ->queryRow();
    }

    public function getCountNew()
    {
        return Yii::app()->db
            ->createCommand("select count(*) as total from feedback where checked  = 0")
            ->queryRow();
    }

    public function update($id)
    {
        $criteria = new CDbCriteria(
            [
                "condition" => "id = :id" ,
                "params" => [
                    "id" => $id,
                ]
            ]
        );

        $update = [
            'checked' => 1
        ];

        $builder = Yii::app()->db->schema->commandBuilder;
        $builder->createUpdateCommand('feedback', $update, $criteria)->execute();
    }
}