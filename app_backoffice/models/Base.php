<?php
class Base extends CModel
{
	public function rules()
	{
		return array();
	}
	
	public function attributeNames()
	{
		return array();
	}
	
	public static function model()
	{
		return new self();
	}
	
	public function getBasesAdminTotal($per_page = 10)
	{
		$func_args = func_get_args();

		if (!empty($func_args[1])) {
			$base_id = (int) $func_args[1];
			$base_title = addcslashes($func_args[1], '%_');

			$total_bases = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM base as b JOIN base_lang as bl ON b.base_id = bl.base_id AND bl.language_code = :code WHERE b.base_id = :id OR bl.base_title LIKE :base_title")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $base_id, PDO::PARAM_INT)
				->bindValue(':base_title', '%' . $base_title . '%', PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$total_bases = Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM base as b JOIN base_lang as bl ON b.base_id = bl.base_id AND bl.language_code = :code")
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryScalar();
		}
		
		return array(
			'total' => (int) $total_bases,
			'pages' => ceil($total_bases / $per_page),
		);
	}

	public function getBasesAdmin($sort, $direction = 'asc', $offset = 0, $per_page = 10)
	{
		switch ($sort) {
			case 'base_id':
				$order_by = ($direction == 'asc') ? 'b.base_id' : 'b.base_id DESC';
				break;
			case 'base_title':
				$order_by = ($direction == 'asc') ? 'bl.base_title' : 'bl.base_title DESC';
				break;
			default:
				$order_by = 'b.base_id DESC';
		}

		$func_args = func_get_args();

		if (!empty($func_args[4])) {
			$base_id = (int) $func_args[4];
			$base_title = addcslashes($func_args[4], '%_');

			$bases = Yii::app()->db
				->createCommand("SELECT b.base_id, b.active, b.base_photo, bl.base_title FROM base as b JOIN base_lang as bl ON b.base_id = bl.base_id AND bl.language_code = :code WHERE b.base_id = :id OR bl.base_title LIKE :base_title ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->bindValue(':id', $base_id, PDO::PARAM_INT)
				->bindValue(':base_title', '%' . $base_title . '%', PDO::PARAM_STR)
				->queryAll();
		}
		else {
			$bases = Yii::app()->db
				->createCommand("SELECT b.base_id, b.active, b.base_photo, bl.base_title FROM base as b JOIN base_lang as bl ON b.base_id = bl.base_id AND bl.language_code = :code ORDER BY " . $order_by . " LIMIT ".$offset.",".$per_page)
				->bindValue(':code', Yii::app()->params->lang, PDO::PARAM_STR)
				->queryAll();
		}
			
		return $bases;
	}

	public function getBaseByIdAdmin($id)
	{
		$base = Yii::app()->db
			->createCommand("SELECT * FROM base WHERE base_id = :id LIMIT 1")
			->bindValue(':id', (int) $id, PDO::PARAM_INT)
			->queryRow();

		if (!empty($base)) {
			// base langs
			$base_langs = Yii::app()->db
				->createCommand("SELECT * FROM base_lang WHERE base_id = :id")
				->bindValue(':id', (int) $id, PDO::PARAM_INT)
				->queryAll();

			if (!empty($base_langs)) {
				foreach ($base_langs as $base_lang) {
					$code = $base_lang['language_code'];

					if (isset(Yii::app()->params->langs[$code])) {
						$base[$code] = $base_lang;
					}
				}
			}
		}

		return $base;
	}

	public function issetBaseByAlias($base_id, $base_alias)
	{
		if (!empty($base_id)) {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM base WHERE base_id != :base_id AND base_alias LIKE :alias")
				->bindValue(':base_id', (int) $base_id, PDO::PARAM_INT)
				->bindValue(':alias', $base_alias, PDO::PARAM_STR)
				->queryScalar();
		}
		else {
			$isset = (bool) Yii::app()->db
				->createCommand("SELECT COUNT(*) FROM base WHERE base_alias LIKE :alias")
				->bindValue(':alias', $base_alias, PDO::PARAM_STR)
				->queryScalar();
		}

		return $isset;
	}

	public function save($model, $model_lang, $artists)
	{
		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		// skip unnecessary attributes
		$skip_attributes = array(
			'base_id',
			'base_photo',
		);

		// integer attributes
		$int_attributes = array();

		// date attributes
		$date_attributes = array();

		// delete attributes
		$del_attributes = array(
			'del_base_photo',
		);

		// photos attributes
		$save_images = array(
			'base_photo',
		);

		$skip_attributes = array_merge($skip_attributes, $del_attributes);

		// get not empty title
		foreach (Yii::app()->params->langs as $language_code => $language_name) {
			if (!empty($model_lang->base_title[$language_code])) {
				$base_title = $model_lang->base_title[$language_code];
				break;
			}
		}

		// get alias
		$model->base_alias = empty($model->base_alias) ? URLify::filter($base_title, 200) : URLify::filter($model->base_alias, 200);

		while ($this->issetBaseByAlias($model->base_id, $model->base_alias)) {
			$model->base_alias = $model->base_alias . '-' . uniqid();
		}

		if (empty($model->base_id)) {
			// insert base
			$insert_base = array(
				'created' => $today,
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$insert_base[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
                        // $insert_base[$field] = '0000-00-00';
                        $date = new DateTime('now', new DateTimeZone(Yii::app()->timeZone));
                        $insert_base[$field] = $date->format('Y-m-d');
                    }
                    else {
                        $date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
                        $insert_base[$field] = $date->format('Y-m-d');
                    }
				}
				else {
					$insert_base[$field] = $value;
				}
			}

			try {
				$rs = $builder->createInsertCommand('base', $insert_base)->execute();

				if ($rs) {
					$model->base_id = (int) Yii::app()->db->getLastInsertID();

					$int_attributes = array(
						'base_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$insert_base_lang = array(
							'base_id' => $model->base_id,
							'language_code' => $language_code,
							'base_visible' => !empty($model_lang->base_title[$language_code]) ? 1 : 0,
							'created' => $today,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$insert_base_lang[$field] = (int) $value[$language_code];
							}
							else {
								$insert_base_lang[$field] = trim($value[$language_code]);
							}
						}

						$rs = $builder->createInsertCommand('base_lang', $insert_base_lang)->execute();

						if (!$rs) {
							$delete_criteria = new CDbCriteria(
								array(
									"condition" => "base_id = :base_id" , 
									"params" => array(
										"base_id" => $model->base_id,
									)
								)
							);
							
							$builder->createDeleteCommand('base', $delete_criteria)->execute();

							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}
		else {
			$update_base = array(
				'saved' => $today,
			);

			foreach ($model as $field => $value) {
				if (in_array($field, $skip_attributes)) {
					continue;
				}
				elseif (in_array($field, $int_attributes)) {
					$update_base[$field] = (int) $value;
				}
				elseif (in_array($field, $date_attributes)) {
					if (empty($value)) {
                        // $update_base[$field] = '0000-00-00';
                        $date = new DateTime('now', new DateTimeZone(Yii::app()->timeZone));
                        $update_base[$field] = $date->format('Y-m-d');
                    }
                    else {
                        $date = new DateTime($value, new DateTimeZone(Yii::app()->timeZone));
                        $update_base[$field] = $date->format('Y-m-d');
                    }
				}
				else {
					$update_base[$field] = $value;
				}
			}

			foreach ($del_attributes as $del_attribute) {
				if (!empty($model->$del_attribute)) {
					$del_attribute = str_replace('del_', '', $del_attribute);
					$update_base[$del_attribute] = '';
				}
			}

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "base_id = :base_id" , 
					"params" => array(
						"base_id" => $model->base_id,
					)
				)
			);

			try {
				$rs = $builder->createUpdateCommand('base', $update_base, $update_criteria)->execute();

				if ($rs) {
					// delete files
					foreach ($del_attributes as $del_attribute) {
						if (!empty($model->$del_attribute)) {
							$file_path = Yii::app()->assetManager->basePath . DS . 'base' . DS . $model->base_id . DS . $model->$del_attribute;

							if (is_file($file_path)) {
								CFileHelper::removeDirectory(dirname($file_path));
							}
						}
					}

					$int_attributes = array(
						'base_no_index',
					);

					foreach (Yii::app()->params->langs as $language_code => $language_name) {
						// save details
						$update_base_lang = array(
							'base_visible' => !empty($model_lang->base_title[$language_code]) ? 1 : 0,
							'saved' => $today,
						);

						foreach ($model_lang->attributes as $field => $value) {
							// checkboxes
							if ($field == 'base_no_index' && !isset($value[$language_code])) {
								$value[$language_code] = 0;
							}

							if (!is_array($value) || !isset($value[$language_code])) {
								// skip non multilang fields
								continue;
							}
							elseif (in_array($field, $int_attributes)) {
								$update_base_lang[$field] = (int) $value[$language_code];
							}
							else {
								$update_base_lang[$field] = trim($value[$language_code]);
							}
						}

						$update_lang_criteria = new CDbCriteria(
							array(
								"condition" => "base_id = :base_id AND language_code = :lang" , 
								"params" => array(
									"base_id" => (int) $model->base_id,
									"lang" => $language_code,
								)
							)
						);

						$rs = $builder->createUpdateCommand('base_lang', $update_base_lang, $update_lang_criteria)->execute();

						if (!$rs) {
							return false;
						}
					}

					// save photos
					$this->savePhotos($model, $save_images);

					return true;
				}
			}
			catch (CDbException $e) {
				// ...
			}
		}

		return false;
	}

	private function savePhotos($model, $attributes)
	{
		if (empty($attributes)) {
			return false;
		}

		// register Imagine namespace
		Yii::setPathOfAlias('Imagine', Yii::getPathOfAlias('application.vendor.Imagine'));

		// import URLify library
		Yii::import('application.vendor.URLify.URLify');

		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$save_images_rs = array();

		if (extension_loaded('imagick')) {
			$imagine = new Imagine\Imagick\Imagine();
		}
		elseif (extension_loaded('gd') && function_exists('gd_info')) {
			$imagine = new Imagine\Gd\Imagine();
		}

		// уменьшит изображение по его большей стороне (будет определено максимальное значение высоты или ширины)
		$mode_inset = Imagine\Image\ImageInterface::THUMBNAIL_INSET;
		// изображение должно полностью поместится в новое разрешение, таким образом все что выйдет за границы области будет обрезано
		$mode_outbound = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

		foreach ($attributes as $attribute) {
			if (!empty($model->$attribute)) {
				$image_id = uniqid();
				$image_parts = explode('.', strtolower($model->$attribute->getName()));
				$image_extension = array_pop($image_parts);
				$image_name = implode('.', $image_parts);
				$image_name = str_replace('-', '_', URLify::filter($image_name, 60, '', true));
				$image_full_name = $image_name . '.' . $image_extension;
				
				$image_dir = Yii::app()->assetManager->basePath . DS . 'base' . DS . $model->base_id . DS . $image_id . DS;
				$image_path = $image_dir . 'tmp_' . $image_full_name;

				$dir_rs = true;

				if (!is_dir($image_dir)) {
					$dir_rs = CFileHelper::createDirectory($image_dir, 0777, true);
				}

				if ($dir_rs) {
					$save_image_rs = $model->$attribute->saveAs($image_path);

					if ($save_image_rs) {
						$image_size = false;
						
						if ($attribute == 'base_photo') {
							$image_files = array();

							$image_file_original = $image_dir . $image_name . '_o.' . $image_extension; // original
							$image_save_name_original = $image_id . '/' . $image_name . '_o.' . $image_extension;

							$image_file_details = $image_dir . $image_name . '_d.' . $image_extension; // details
							$image_save_name_details = $image_id . '/' . $image_name . '_d.' . $image_extension;
							$image_file_details_2x = $image_dir . $image_name . '_d@2x.' . $image_extension; // details 2x
							$image_save_name_details_2x = $image_id . '/' . $image_name . '_d@2x.' . $image_extension;

							$image_file_list = $image_dir . $image_name . '_l.' . $image_extension; // list
							$image_save_name_list = $image_id . '/' . $image_name . '_l.' . $image_extension;
							$image_file_list_2x = $image_dir . $image_name . '_l@2x.' . $image_extension; // list 2x
							$image_save_name_list_2x = $image_id . '/' . $image_name . '_l@2x.' . $image_extension;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							if ($original_image_size->getWidth() < 364 || $original_image_size->getHeight() < 250) {
								continue;
							}

							$resized_image = $image_obj->save($image_file_original, array('quality' => 80));

							$image_files['original'] = array(
								'path' => $image_save_name_original,
								'size' => array(
									'w' => $resized_image->getSize()->getWidth(),
									'h' => $resized_image->getSize()->getHeight(),
								),
							);

							// details image
							$image_obj = $imagine->open($image_path);
							$resized_image_2x = null;

							if ($original_image_size->getWidth() >= 2240 && $original_image_size->getHeight() >= 1260) {
								$resized_image_2x = $image_obj->thumbnail(new Imagine\Image\Box(2240, 1260), $mode_outbound)
									->save($image_file_details_2x, array('quality' => 80));
							}
							
							if ($original_image_size->getWidth() >= 1120 && $original_image_size->getHeight() >= 630) {
								$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(1120, 630), $mode_outbound)
									->save($image_file_details, array('quality' => 80));
								/* $resized_image = $image_obj->resize($original_image_size->widen(1120))
									->save($image_file_details, array('quality' => 80)); */
							} else {
								$resized_image = $image_obj->save($image_file_details, array('quality' => 80));
							}

							$image_files['details'] = [
								'1x' => array(
									'path' => $image_save_name_details,
									'size' => array(
										'w' => $resized_image->getSize()->getWidth(),
										'h' => $resized_image->getSize()->getHeight(),
									),
								),
								'2x' => empty($resized_image_2x) ? null : array(
									'path' => $image_save_name_details_2x,
									'size' => array(
										'w' => $resized_image_2x->getSize()->getWidth(),
										'h' => $resized_image_2x->getSize()->getHeight(),
									),
								),
							];

							// list image
							$image_obj = $imagine->open($image_path);
							$resized_image_2x = null;

							if ($original_image_size->getWidth() >= 728 && $original_image_size->getHeight() > 500) {
								$resized_image_2x = $image_obj->thumbnail(new Imagine\Image\Box(728, 500), $mode_outbound)
									->save($image_file_list_2x, array('quality' => 80));
							}

							$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(364, 250), $mode_outbound)
								->save($image_file_list, array('quality' => 80));

							$image_files['list'] = [
								'1x' => array(
									'path' => $image_save_name_list,
									'size' => array(
										'w' => $resized_image->getSize()->getWidth(),
										'h' => $resized_image->getSize()->getHeight(),
									),
								),
								'2x' => empty($resized_image_2x) ? null : array(
									'path' => $image_save_name_list_2x,
									'size' => array(
										'w' => $resized_image_2x->getSize()->getWidth(),
										'h' => $resized_image_2x->getSize()->getHeight(),
									),
								),
							];

							if (is_file($image_file_original) && is_file($image_file_details) && is_file($image_file_list)) {
								$save_images_rs[$attribute] = json_encode($image_files);
							}
						}
						else {
							$image_file = $image_dir . $image_name;
							$image_save_name = $image_id . '/' . $image_name;

							// resize file
							$image_obj = $imagine->open($image_path);
							$original_image_size = $image_obj->getSize();

							switch ($attribute) {
								case 'base_photo':
									if ($original_image_size->getWidth() > 300 && $original_image_size->getHeight() > 300) {
										$resized_image = $image_obj->thumbnail(new Imagine\Image\Box(300, 300), $mode_outbound)
											->save($image_file, array('quality' => 80));
									}
									else {
										$resized_image = $image_obj->save($image_file, array('quality' => 80));	
									}
									break;
							}

							$image_size = array(
								'w' => $resized_image->getSize()->getWidth(),
								'h' => $resized_image->getSize()->getHeight(),
							);

							if (is_file($image_file)) {
								$save_images_rs[$attribute] = json_encode(array_merge(
									array(
										'file' => $image_save_name,
									),
									$image_size
								));
							}
							else {
								// remove resized files
								if (is_file($image_file)) {
									unlink($image_file);
								}
							}
						}

						// remove original image
						unlink($image_path);
					}
				}
			}
		}

		if (!empty($save_images_rs)) {
			$update_base = array(
				'saved' => $today,
			);

			$update_base = array_merge($update_base, $save_images_rs);

			$update_criteria = new CDbCriteria(
				array(
					"condition" => "base_id = :base_id" , 
					"params" => array(
						"base_id" => $model->base_id,
					)
				)
			);

			try {
				$save_photo_rs = (bool) $builder->createUpdateCommand('base', $update_base, $update_criteria)->execute();
			}
			catch (CDbException $e) {
				// ...
			}
		}
	}

	public function toggle($base_id, $active)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_base = array(
			'saved' => $today,
			'active' => (int) $active,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "base_id = :base_id" , 
				"params" => array(
					"base_id" => $base_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('base', $update_base, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function setPosition($base_id, $position)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$today = date('Y-m-d H:i:s');

		$update_base = array(
			'saved' => $today,
			'base_position' => (int) $position,
		);

		$update_criteria = new CDbCriteria(
			array(
				"condition" => "base_id = :base_id" , 
				"params" => array(
					"base_id" => $base_id,
				)
			)
		);

		try {
			$rs = $builder->createUpdateCommand('base', $update_base, $update_criteria)->execute();

			if ($rs) {
				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}

	public function delete($base_id)
	{
		$builder = Yii::app()->db->schema->commandBuilder;
		$assetPath = Yii::app()->assetManager->basePath;

		$base = $this->getBaseByIdAdmin($base_id);
		
		$delete_criteria = new CDbCriteria(
			array(
				"condition" => "base_id = :base_id" , 
				"params" => array(
					"base_id" => $base_id,
				)
			)
		);
		
		try {
			$rs = $builder->createDeleteCommand('base', $delete_criteria)->execute();

			if ($rs) {
				// delete related tables
				$builder->createDeleteCommand('base_lang', $delete_criteria)->execute();

				// remove base directory
				if (is_dir($assetPath . DS . 'base' . DS . $base_id)) {
					CFileHelper::removeDirectory($assetPath . DS . 'base' . DS . $base_id);
				}

				return true;
			}
		}
		catch (CDbException $e) {
			// ...
		}

		return false;
	}
}