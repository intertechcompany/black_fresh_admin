<?php
	$assetsUrl = Yii::app()->assetManager->getBaseUrl();
?>
<html>
	<head>
		<title><?=Lang::t('pdf.title.offer', array('{offer_id}' => $offer['offer_id']))?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			body {
				color: #43484d;
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
			}

			table {
				border-collapse: collapse;
				border-spacing: 0;
			}

			td,
			th {
				padding: 0;
			}

			.invoice-title-sep {
				border-top: 2px solid #43484d;
				padding-top: 10px;
				margin: 20px 0 30px;
			}

			.product th,
			.product td,
			.product-bottom td {
				padding: 10px;
				font-size: 12px;
				vertical-align: middle;
			}

			.product th,
			.product td,
			.product-bottom td {
				padding-left: 7px;
				padding-right: 7px;
			}

			.product {
				margin-top: 10px;
				margin-bottom: 10px;
			}

			.product th {
				border-bottom: 2px solid #43484d;
			}

			.product td {
				border-bottom: 1px solid #43484d;
			}

			.product a {
				color: #43484d;
				text-decoration: underline;
			}

			.product-bottom td {
				font-size: 14px;
			}
		</style>
	</head>
	<body>
		<div class="invoice-head">
			<table style="width: 100%;" autosize="1">
				<tr>
					<td><img src="<?=$assetsUrl . '/inorex_oy.svg'?>" width="200" height="46" alt="Inorex"></td>
					<td style="font-size: 20px; text-align: right; vertical-align: bottom;">
						<b><?=Lang::t('pdf.title.offer', array('{offer_id}' => $offer['offer_id']))?></b>
					</td>
				</tr>
			</table>
		</div>
		
		<div class="invoice-title-sep">&nbsp;</div>
		
		<div class="invoice-details">
			<table style="width: 100%;" autosize="1">
				<tbody>
					<tr>
						<td width="50%" style="padding: 0 40px 60px 0; vertical-align: top">
							<?=str_replace(array("\r", "\n"), array("", "<br>\n"), Lang::t('pdf.tip.inorexContacts'))?>
						</td>
						<td width="50%" style="padding: 0 0 40px 60px; vertical-align: top">
							<?php if (!empty($seller)) { ?>
							<b><?=Lang::t('pdf.tip.seller')?></b><br>
							<?=CHtml::encode($seller['user_first_name'] . ' ' . $seller['user_last_name'])?><br>
							<?=CHtml::encode($seller['user_phone'])?><br>
							<?=CHtml::encode($seller['user_email'])?>
							<?php } ?>
						</td>
					</tr>
					<tr>
						<td width="50%" style="padding: 0 40px 60px 0; vertical-align: top">
							<b><?=CHtml::encode($offer['first_name'] . ' ' . $offer['last_name'])?></b><br>
							<?php if (!empty($offer['address_2'])) { ?>
							<?=CHtml::encode($offer['address_2'])?><br>
							<?php } ?>
		
							<?=CHtml::encode($offer['address'])?><br>
							<?=CHtml::encode($offer['zip'] . ' ' . $offer['city'])?><br>
							<?=CHtml::encode($offer['phone'])?><br>
							<?=CHtml::encode($offer['email'])?>
						</td>
						<td width="50%" style="padding: 0 0 40px 60px; vertical-align: top">
							<?php if (!empty($offer['delivery_to_address'])) { ?>
							<b><?=Lang::t('pdf.tip.deliveryTo')?></b><br>
							<?=CHtml::encode($offer['delivery_first_name'] . ' ' . $offer['delivery_last_name'])?><br>
							<?php if (!empty($offer['delivery_address_2'])) { ?>
							<?=CHtml::encode($offer['delivery_address_2'])?><br>
							<?php } ?>
		
							<?=CHtml::encode($offer['delivery_address'])?><br>
							<?=CHtml::encode($offer['delivery_zip'] . ' ' . $offer['delivery_city'])?><br>
							<?=CHtml::encode($offer['delivery_phone'])?><br>
							<?=CHtml::encode($offer['delivery_email'])?>
							<?php } ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<div class="product">
			<table style="width: 100%;" autosize="1">
				<thead>
					<tr>
						<th style="width: 16%">&nbsp;</th>
						<th style="width: 34%; text-align: left"><?=Lang::t('pdf.tip.thProduct')?></th>
						<th style="width: 10%"><?=Lang::t('pdf.tip.thQuantity')?></th>
						<th style="width: 10%"><?=Lang::t('pdf.tip.thUnit')?></th>
						<th style="width: 10%"><?=Lang::t('pdf.tip.thUnitPrice')?></th>
						<th style="width: 10%"><?=Lang::t('pdf.tip.thTotal')?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($products as $product) { ?>
					<?php
						$product_image = '';
		
						if (!empty($product['product_alias'])) {
							$product_url = $this->createAbsoluteUrl('site/product', array('alias' => $product['product_alias']));
		
							if (!empty($product['product_photo'])) {
								$product_image = json_decode($product['product_photo'], true);
								$product_image_size = $product_image['size']['catalog'];
								$product_image = $assetsUrl . '/product/' . $product['product_id'] . '/' . $product_image['path']['catalog'];
								$product_image = '<a href="' . $product_url . '"><img src="' . $product_image . '" width="' . $product_image_size['w'] . '" height="' . $product_image_size['h'] . '" style="max-width: 70px; max-height: 70px; width: auto; height: auto;"></a>';
							}

							$variant_options = '';

							if (!empty($product['variant'])) {
								$product['product_sku'] = $product['variant']['variant_sku'];

								if (!empty($product['variant']['values'])) {
									$variant_values = array();

									foreach ($product['variant']['values'] as $value) {
										$variant_values[] = CHtml::encode($value['property_title'] . ': ' . $value['value_title']);
									}
								
									$variant_options = '<br><span style="font-size: 10px;">' . implode("<br>\n", $variant_values) . '</span>';
								}
							}

							$pack_size = (float) $product['product_pack_size'];
							$pack_size = empty($pack_size) ? 1 : $pack_size;

							if ($product['product_price_type'] == 'package') {
								$unit_price = round($product['price'] / $pack_size, 2);
								$unit = 'm<sup>2</sup>';
							} elseif ($product['product_price_type'] == 'per_meter') {
								if ($pack_size > 1) {
									$unit_price = round($product['price'] / $pack_size, 2);
								} else {
									$unit_price = $product['price'];
								}

								$unit = 'm';
							} else {
								$unit_price = $product['price'];
								$unit = Lang::t('checkout.tip.pcs');
							}
		
							$product_title = '<a href="' . $product_url . '">' . CHtml::encode($product['product_title']) . '</a><br><span style="font-size: 10px; color: #aaa">' . Lang::t('accountOrder.tip.productCode') . ' ' . $product['product_sku'] . '</span>' . $variant_options;
						} else {
							$pack_size = 1;
							$unit_price = $product['price'];
							$unit = Lang::t('checkout.tip.pcs');

							$product_title = CHtml::encode($product['title']);

							if (!empty($product['variant_title'])) {
								$product_title .= '<br><span style="color: #aaa">' . str_replace("\n", "<br>\n", CHtml::encode($product['variant_title'])) . '</span>';
							}
						}
					?>
					<tr>
						<td style="text-align: center;"><?=$product_image?></td>
						<td style="text-align: left;"><?=$product_title?></td>
						<td style="text-align: center;"><?=number_format($pack_size * $product['quantity'], 2, '.', '')?></td>
						<td style="text-align: center;"><?=$unit?></td>
						<td style="text-align: center;"><?=number_format($unit_price, 2, '.', '')?>€</td>
						<td style="text-align: center;"><?=number_format($product['price'] * $product['quantity'], 2, '.', '')?>€</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="product-bottom">
			<table style="width: 100%;" autosize="1">
				<tr>
					<td style="width: 90%; text-align: right; padding-bottom: 5px"><?=Lang::t('accountOrder.title.alv')?></td>
					<td style="width: 10%; text-align: center; padding-bottom: 5px"><b><?=number_format(round($offer['price'] * 0.24, 2), 2, '.', '')?>€</b></td>
				</tr>
				<tr>
					<td style="width: 90%; text-align: right; padding-top: 5px"><?=Lang::t('accountOrder.title.totalAmount')?></td>
					<td style="width: 10%; text-align: center; padding-top: 5px"><b><?=$offer['price']?>€</b></td>
				</tr>
			</table>
		</div>

		<pagebreak />

		<div class="offer-terms">
			<?=Lang::t('pdf.html.offerTermsAndConditions')?>
		</div>
	</body>
</html>