<?php
/* @var $this AdminController */
?>
<?php
	$managers_url_data = array();

	if (!empty($sort)) {
		$managers_url_data['sort'] = $sort;
		$managers_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$managers_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$managers_url_data['page'] = $page;
	}

	$back_link = $this->createUrl('managers', $managers_url_data);

	$has_rights = Yii::app()->getUser()->hasAccess($this->route, true);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>

<form id="manage-manager" class="form-horizontal" method="post" enctype="multipart/form-data">
	<div class="manager-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>
	
	<div class="form-group">
		<label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
		<div class="col-md-4">
			<select id="form-active" name="manager[active]" class="form-control">
				<option value="0"><?=Yii::t('managers', 'Blocked')?></option>
				<option value="1"><?=Yii::t('managers', 'Active')?></option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_login" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager login')?>:</label>
		<div class="col-md-4 control-required">
			<input id="form-manager_login" class="form-control" type="text" name="manager[manager_login]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_password" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager password')?>:</label>
		<div class="col-md-4 control-required">
			<input id="form-manager_password" class="form-control" type="text" name="manager[manager_password]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_email" class="col-md-3 control-label">Email:</label>
		<div class="col-md-4">
			<input id="form-manager_email" class="form-control" type="text" name="manager[manager_email]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_first_name" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager name')?>:</label>
		<div class="col-md-4">
			<input id="form-manager_first_name" class="form-control" type="text" name="manager[manager_first_name]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_middle_name" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager middle name')?>:</label>
		<div class="col-md-4">
			<input id="form-manager_middle_name" class="form-control" type="text" name="manager[manager_middle_name]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_last_name" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager last name')?>:</label>
		<div class="col-md-4">
			<input id="form-manager_last_name" class="form-control" type="text" name="manager[manager_last_name]" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="form-manager_roles" class="col-md-3 control-label"><?=Yii::t('managers', 'Manager roles')?>:</label>
		<div class="col-md-6">
			<select id="form-manager_roles" class="form-control select2" name="roles[]" multiple data-placeholder="Выберите роли...">
				<?php if (!empty($roles)) { ?>
				<?php foreach ($roles as $role) { ?>
				<option value="<?=$role['role_id']?>"><?=CHtml::encode($role['role_name'])?></option>
				<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"<?php if (!$has_rights) { ?> disabled<?php } ?>><?=Yii::t('app', 'Add btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
		</div>
	</div>
</form>

<script>
var is_manager = true;

$(document).ready(function(){
	<?php if (!$has_rights) { ?>
	$('form').find(':input').prop('disabled', true);
	<?php } else {
        ?>
	
	var form = $("#manage-manager"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				var that = this,
					alert_callback = function() {
						if (that.id == 'form-redactor') {
							setTimeout(function() {
								$(that).redactor('focus.setStart');
							}, 21);
						}
						else {
							setTimeout(function() {
								$(that).focus();
							}, 21);
						}
					};

				switch (this.id) {
					case 'form-manager_login':
						bootbox.alert("<?=Yii::t('managers', 'Enter a manager login!')?>", alert_callback);
						break;
					case 'form-manager_email':
						bootbox.alert("<?=Yii::t('managers', 'Enter a manager email!')?>", alert_callback);
						break;
					case 'form-manager_password':
						bootbox.alert("<?=Yii::t('managers', 'Enter a manager password!')?>", alert_callback);
						break;
				}

				return false;
			}
		});
			
		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});

	<?php } ?>
});
</script>