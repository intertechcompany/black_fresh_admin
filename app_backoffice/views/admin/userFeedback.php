<?php
/* @var $this AdminController */
?>
<h1>User Feedback <!-- Настройки --></h1>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover" style="cursor: pointer">
                <thead>
                    <tr id="main">
                        <th scope="col">#</th>
                        <th scope="col"><?=Lang::t('feedback.page.user')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.reason')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.message')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.created')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.file')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.view')?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($feedback as $item) {?>
                    <tr id="<?php echo $item['id'] ?>">
                        <th scope="row"><?php echo $item['id'] ?></th>
                        <td><a href="<?php echo $this->createUrl('user', ['id' => $item['user_id']]) ?>" target="_blank"><?php echo $item['user_first'].' '.$item['user_last'] ?></a></td>
                        <td><?php echo $item['reason'] ?></td>
                        <td><?php echo $item['message'] ?></td>
                        <td><?php echo $item['created'] ?></td>
                        <td>
                            <?php foreach ($files as $file) {
                                if ($file['feedback_id'] == $item['id']) {
                            ?>
                                    <a href="<?php echo Yii::app()->assetManager->getBaseUrl().'/uploads/' . $file['file']; ?>" target="_blank">Файл</a>
                                <?php } ?>
                            <?php } ?>
                        </td>
                        <td><?php if ($item['checked']){ echo 'Да'; } else { echo 'Нет'; }?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php if ($total > 1) { ?>
                <div class="pages text-center">
                    <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'pagination',
                        ),
                    ));
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    $('tr').click(function () {
        const element = $("#" + this.id);

        if (element.attr('id') !== 'main') {
            checked(element.attr('id'));
        }
    })

    function checked(id)
    {
        $.post( "/approve-userFeedback", {id: id} );

        location.reload();
    }
</script>