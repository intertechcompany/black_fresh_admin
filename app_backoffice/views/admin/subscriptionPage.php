<?php
/* @var $this AdminController */
?>
<h1><?=Lang::t('subscription.page.title')?> <!-- Настройки --></h1>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <table class="table" style="cursor: pointer">
                <thead>
                    <tr id="main">
                        <th scope="col"><?=Lang::t('subscription.page.user')?></th>
                        <th scope="col"><?=Lang::t('subscription.page.first.pay')?></th>
                        <th scope="col"><?=Lang::t('subscription.page.payment.status')?></th>
                        <th scope="col"><?=Lang::t('subscription.page.delivery.status')?></th>
                        <th scope="col"><?=Lang::t('subscription.page.price')?></th>
                        <th scope="col"><?=Lang::t('subscription.page.date.delivery')?></th>
                        <th scope="col"><?=Lang::t('subscription.page.date.created')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($subs as $sub) {
                        $bg = '';
                        if (isset($sub['payment_status']) && $sub['first_pay'] == 1) {
                            if ($sub['delivery_status'] == 'checked') {
                                $bg = 'bg-success';
                            } elseif ($sub['payment_status'] == 'Declined') {
                                $bg = 'bg-warning';
                            } else {
                                $bg = 'bg-primary';
                            }
                        } elseif (isset($sub['snp_payment'])) {
                            $json = json_decode($sub['snp_payment']);
                            if (isset($json->reason) && $json->reason == 'Ok') {
                                if ($sub['delivery_status'] == 'checked') {
                                    $bg = 'bg-success';
                                } else {
                                    $bg = 'bg-info';
                                }
                            } else {
                                if ($sub['delivery_status'] == 'checked') {
                                    $bg = 'bg-success';
                                } elseif (isset($json->reason) && $json->reason == "Regular payment is completed" && $json->status == "Completed") {
                                    $bg = 'bg-primary';
                                } else {
                                    $bg = 'bg-danger';
                                }
                            }
                        } else {
                            if ($sub['delivery_status'] == 'checked') {
                                $bg = 'bg-success';
                            } else {
                                $bg = 'bg-warning';
                            }
                        }
                    ?>
                        <tr id="<?php echo $sub['id']; ?>"
                            class="<?php echo $bg; ?>"
                            data-name="<?php echo $sub['user_first_name'] ?>" data-surname="<?php echo $sub['user_last_name'] ?>"
                            data-phone="<?php echo $sub['phone'] ?>" data-product="<?php echo $sub['title'] ?>"
                            data-option="<?php echo $sub['option_title'] ?>" data-variant="<?php echo $sub['variant_title'] ?>"
                            data-quantity="<?php echo $sub['quantity'] ?>" data-delivery="<?php echo $sub['delivery'] ?>"
                            data-delivery-address="<?php echo $sub['delivery_address'] ?>" data-city="<?php echo $sub['np_city'] ?>"
                            data-department="<?php echo $sub['np_department'] ?>" data-order="<?php echo $sub['order_reference'] ?>"
                            data-milling="<?php echo $sub['milling'] ?>"
                        >
                            <td><?php echo $sub['user_first_name'] . " " . $sub['user_last_name'] ?></td>
                            <td><?php if ($sub['first_pay'] == 1) { echo "Да";} else { echo "Нет"; }?></td>
                            <td>
                                <?php
                                if (isset($sub['payment_status']) && $sub['first_pay'] == 1) {
                                    if ($sub['payment_status'] == "Approved") {
                                        echo "Оплачено";
                                    } else {
                                        echo $sub['payment_status'];
                                    }
                                } elseif (isset($sub['snp_payment'])) {
                                    $json = json_decode($sub['snp_payment']);
                                    if (isset($json->reason) && $json->reason == 'Ok') {
                                        echo "Создано регулярный платеж";
                                    } elseif (isset($json->reason) && $json->reason == "Regular payment is completed" && $json->status == "Completed") {
                                        echo "Регулярнный плятеж оплачен";
                                    } else {
                                        if (isset($json->reason)) {
                                            echo $json->reason;
                                        }
                                    }
                                } else {
                                    echo "Ожидание";
                                }
                                ?>
                            </td>
                            <td id="delivery-status-<?php echo $sub['id']; ?>">
                                <?php
                                    if ($sub['delivery_status'] == 'new') {
                                        echo  "Не отправлено";
                                    } elseif ($sub['delivery_status'] == 'checked') {
                                        echo "Отправлено";
                                    }
                                ?>
                            </td>
                            <td><?php echo $sub['price']; ?></td>
                            <td><?php echo $sub['next_date']; ?></td>
                            <td><?php echo $sub['snp_created']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php if ($total > 1) { ?>
                <div class="pages text-center">
                    <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'pagination',
                        ),
                    ));
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bd-example-modal-sm" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover" style="cursor: pointer">
                    <tbody>
                        <tr>
                            <td><?=Lang::t('subscription.page.user.name')?></td>
                            <td id="name"></td>
                        </tr>
                        <tr>
                            <td><?=Lang::t('subscription.page.user.surname')?></td>
                            <td id="surname"></td>
                        </tr>
                        <tr>
                            <td><?=Lang::t('subscription.page.user.phone')?></td>
                            <td id="phone"></td>
                        </tr>
                        <tr>
                            <td><?=Lang::t('subscription.page.product')?></td>
                            <td id="product"></td>
                        </tr>
                        <tr>
                            <td><?=Lang::t('subscription.page.product.option')?></td>
                            <td id="option"></td>
                        </tr>
                        <tr>
                            <td><?=Lang::t('subscription.page.product.variant')?></td>
                            <td id="variant"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td id="milling"></td>
                        </tr>
                        <tr>
                            <td><?=Lang::t('subscription.page.product.count')?></td>
                            <td id="count"></td>
                        </tr>
                        <tr>
                            <td><?=Lang::t('subscription.page.product.delivery')?></td>
                            <td id="delivery"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=Lang::t('subscription.page.btn.close')?></button>
                <button type="button" id="approve-btn" onclick="update()" data-id="" class="btn btn-primary"><?=Lang::t('subscription.page.btn.approve')?></button>
            </div>
        </div>
    </div>
</div>


<script>
    $('tr').click(function () {
        const element = $("#" + this.id);

        if (element.attr('id') !== 'main') {
            $('#name').text(element.data('name'));
            $('#surname').text(element.data('surname'));
            $('#phone').text(element.data('phone'));
            $('#product').text(element.data('product'));
            $('#option').text(element.data('option'));
            $('#variant').text(element.data('variant'));
            $('#count').text(element.data('quantity'));
            $('#milling').text(element.data('milling'));

            if (element.data('delivery') === 2) {
                $('#delivery').text("Кур'єр: " + element.data('delivery-address'));
            } else if (element.data('delivery') === 3) {
                $('#delivery').text("Нова пошта: " + element.data('city') + ". " + element.data('department'));
            } else if (element.data('delivery') === 4) {
                $('#delivery').text("Нова пошта: м. " + element.data('city') + ", " + element.data('delivery-address') + " " + element.data('department'));
            }
            $('#approve-btn').attr('data-id', this.id);
            $('#modal').modal('show');
        }
    });

    function update() {
        let id = $('#approve-btn').attr('data-id');
        let data = {};
        data.order =  id;

        document.getElementById(id).className = 'bg-success';
        $("#delivery-status-" + id).text("Отправлено");

        $.post( "/approve-sub", data );
    }
</script>
