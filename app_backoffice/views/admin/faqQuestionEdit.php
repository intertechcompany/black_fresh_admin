<?php
/* @var $this AdminController */
?>
<?php
	$back_link = $this->createUrl('faqQuest');
?>
<h1>FAQ Вопрос</h1>

<p class="text-center">
	<a href="<?=$back_link?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('app', 'Back to the list')?></a>
</p>
<div class="text-center">
    <form method="post">
        <input type="hidden" name="func" value="delete">
        <button type="submit" class="btn  btn-danger">Удалить</button>
    </form>
</div>
<form id="manage-page" class="form-horizontal" method="post" enctype="multipart/form-data">
	<div class="page-header">
		<h3><?=Yii::t('app', 'General fields')?></h3>
	</div>

    <div class="form-group">
        <label for="form-active" class="col-md-3 control-label"><?=Yii::t('app', 'Status')?>:</label>
        <div class="col-md-3">
            <select id="form-active" name="faq[active]" class="form-control">
                <option value="0"><?=Yii::t('faq', 'Blocked')?></option>
                <option value="1" <?php if ($faqQuest['active'] == 1) { echo "selected"; } ?> ><?=Yii::t('faq', 'Active')?></option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="form-active" class="col-md-3 control-label">Группа:</label>
        <div class="col-md-3">
            <select id="form-active" name="faq[group_id]" class="form-control">
                <?php foreach ($faqGroups as $group) { ?>
                        <?php if ($faqQuest['group_id'] == $group['faq_id']) { ?>
                            <option value="<?php echo $group['faq_id']; ?>" selected><?php echo $group['faq_title']; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $group['faq_id']; ?>"><?php echo $group['faq_title']; ?></option>
                        <?php } ?>
                <?php } ?>
            </select>
        </div>
    </div>

	<div class="form-group">
		<label for="form-page_title" class="col-md-3 control-label">Вопрос:</label>
		<div class="col-md-6 control-required">
			<div class="lang-tabs" role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
						<a href="#form-page_title_<?=$code?>" aria-controls="form-page_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
					</li>
					<?php } ?>
				</ul>
				<div class="tab-content">
					<?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
					<div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_title_<?=$code?>">
						<input class="form-control" type="text" name="faq_lang[quest_title][<?=$code?>]" value="<?php echo $faqQuest[$code]['quest_title']; ?>">
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

    <div class="form-group">
        <label for="form-page_title" class="col-md-3 control-label">Ответ:</label>
        <div class="col-md-6 control-required">
            <div class="lang-tabs" role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <li role="presentation"<?php if ($code == Yii::app()->params->lang) { ?> class="active"<?php } ?>>
                            <a href="#form-page_desc_<?=$code?>" aria-controls="form-page_title_<?=$code?>" role="tab" data-toggle="tab"><?=CHtml::encode($lang)?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <?php foreach (Yii::app()->params->langs as $code => $lang) { ?>
                        <div role="tabpanel" class="tab-pane<?php if ($code == Yii::app()->params->lang) { ?> active<?php } ?>" id="form-page_desc_<?=$code?>">
                            <textarea class="form-control form-redactor" name="faq_lang[quest_description][<?=$code?>]"><?php echo $faqQuest[$code]['quest_description']; ?></textarea>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

	<hr>
	
	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<button type="submit" class="btn btn-primary btn-lg"><?=Yii::t('app', 'Update btn')?></button>
			<a id="cancel" href="<?=$back_link?>" class="btn btn-link"><?=Yii::t('app', 'Cancel btn')?></a>
        </div>
	</div>
</form>


<script>
$(document).ready(function(){
	var form = $("#manage-page"),
		required_input = form.find('.control-required input, .control-required select, .control-required textarea');

	form.find('input, textarea').focus(function() {
		focused = $(this);
	});

	$('#form-page_type').on('change', function() {
		var value = $(this).val();

		if (value == 'about') {
			$('.page-about').removeClass('hidden');
			$('.page-text').addClass('hidden');
		} else {
			$('.page-about').addClass('hidden');
			$('.page-text').removeClass('hidden');
		}
	}).change();

	$('#typograph-link-btn').on('click', function(e) {
		var input = focused,
			text = input.val(),
			btn = $(this);

		if (focused != null) {
			btn.prop('disabled', true);

			$.ajax({type: "POST", url: '<?=Yii::app()->getBaseUrl(true)?>/ajax/typograph', data: $.param({text: focused.val()}), cache: false, dataType: "json",
				success: function(data) {
					btn.prop('disabled', false);

					if (data.error == 'Y') {
						bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");
					}
					else {
						if (focused.hasClass('form-redactor')) {
							$('#' + focused[0].id).redactor('code.set', data.text);
						}
						else {
							focused.val(data.text);
						}
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					btn.prop('disabled', false);

					bootbox.alert("<?=Yii::t('app', 'Typograph request error')?>");

					// console.log('Loader Error:\n' + textStatus + ' ' + errorThrown );
				}
			});
		}
	});

	$('#insert-link-btn').on('click', function(e) {
		var input = focused,
			len = input.val().length,
			start = input[0].selectionStart,
			end = input[0].selectionEnd,
			selectedText = input.val().substring(start, end);

		$('#link-text').val(selectedText);
		$('#link-url').val('');
		$('#link-title').val('');
		$('#link-blank').prop('checked', false);

		if (focused != null) {
			$('#insertLinkModal').modal('show');
		}
	});

	$('#remove-link-btn').on('click', function(e) {
		if (focused != null) {
			var input = focused,
				new_value = input.val().replace(/<a[^>]*>([\s\S]*?)<\/a>/ig, '$1');

			input.val(new_value);
		}
	});

	$('#insert-link').submit(function() {
		var link_url = $.trim($('#link-url').val()),
			link_text = $.trim($('#link-text').val()),
			link_title = $.trim($('#link-title').val()),
			link_blank = $('#link-blank').prop('checked');

		if (link_url == '' || link_text == '') {
			$('#insertLinkModal').modal('hide');
			return false;
		}

		var new_link = $('<a href="' + link_url + '"/>');
		new_link.text(link_text);

		if (link_title != '') {
			new_link.attr('title', link_title);
		}

		if (link_blank) {
			new_link.attr('target', '_blank');
		}

		var len = focused.val().length,
			start = focused[0].selectionStart,
			end = focused[0].selectionEnd,
			selectedText = focused.val().substring(start, end);

		focused.val(focused.val().substring(0, start) + new_link[0].outerHTML + focused.val().substring(end, len));
		focused = null;

		$('#insertLinkModal').modal('hide');

		return false;
	});

	$('#do-insert').click(function() {
		$('#insert-link').submit();

		return false;
	});

	$('#cancel').click(function() {
		form_submit = true;
	});

	$(window).on('beforeunload', function() {
		if (form_changed && !form_submit) {
			return '<?=Yii::t('app', 'The form data will not be saved!')?>';
		}
	});

	form.one('change', 'input, select, textarea', function(){
		form_changed = true;
	});

	form.submit(function(e) {
		var error = false;

		required_input.each(function(){
			if ($.trim($(this).val()) == '') {
				error = true;

				// error for lang fields
				if ($(this).parent().hasClass('tab-pane')) {
					var that = this,
						tab = $(this).parent().parent().prev().children(':eq(' + $(this).parent().index() + ')').children(),
						alert_callback = function() {
							if (that.id == 'form-page_description') {
								setTimeout(function() {
									tab.click();
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									tab.click();
									$(that).focus();
								}, 21);
							}
						};

					if ($(this).parent()[0].id.indexOf('form-page_title') === 0) {
						bootbox.alert("<?=Yii::t('faq', 'Enter a group title!')?>", alert_callback);
					}
				}
				else {
					var that = this,
						alert_callback = function() {
							if (that.id == 'form-redactor') {
								setTimeout(function() {
									$(that).redactor('focus.setStart');
								}, 21);
							}
							else {
								setTimeout(function() {
									$(that).focus();
								}, 21);
							}
						};

					switch (this.id) {
						case 'form-page_title':
							bootbox.alert("<?=Yii::t('pages', 'Enter a page title!')?>", alert_callback);
							break;
					}
				}

				return false;
			}
		});

		if (error) {
			return false;
		}
		else {
			form_submit = true;
		}
	});
});
</script>