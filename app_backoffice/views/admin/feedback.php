<?php
/* @var $this AdminController */
?>
<h1>Feedback <!-- Настройки --></h1>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover" style="cursor: pointer">
                <thead>
                    <tr id="main">
                        <th scope="col">#</th>
                        <th scope="col"><?=Lang::t('feedback.page.page')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.email')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.is.send')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.date')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.view')?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($feedback as $item) {?>
                    <tr id="<?php echo $item['id'] ?>" data-name="<?php echo $item['first_name'] ?>" data-surname="<?php echo $item['last_name'] ?>" data-phone="<?php echo $item['phone'] ?>"
                        data-message="<?php echo $item['comment'] ?>"
                    >
                        <th scope="row"><?php echo $item['id'] ?></th>
                        <td><a href="<?php echo $this->createUrl('page', ['id' => $item['page_id']]) ?>" target="_blank"><?php echo $item['page_title'] ?></a></td>
                        <td><?php echo $item['email'] ?></td>
                        <td><?php if ($item['is_send']){ echo 'Да'; } else { echo 'Нет'; }?></td>
                        <td><?php echo $item['created'] ?></td>
                        <td><span id="checked-<?php echo $item['id'] ?>"><?php if ($item['checked']){ echo 'Да'; } else { echo 'Нет'; }?></span></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php if ($total > 1) { ?>
                <div class="pages text-center">
                    <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'pagination',
                        ),
                    ));
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-sm" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?=Lang::t('feedback.page.info')?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
                <div class="card">
                    <div class="card-body">
                        <h4 class=""><?=Lang::t('feedback.page.name')?>: <span id="modal-name"></span></h4>
                        <h4 class=""><?=Lang::t('feedback.page.surname')?>: <span id="modal-surname"></span></h4>
                        <h4 class=""><?=Lang::t('feedback.page.phone')?>: <span id="modal-phone"></span></h4>
                        <h4><?=Lang::t('feedback.page.message')?>: <span id="modal-text"></span></h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=Lang::t('feedback.modal.close')?></button>
            </div>
        </div>
    </div>
</div>

<script>
    $('tr').click(function () {
        const element = $("#" + this.id);

        if (element.attr('id') !== 'main') {
            $('#modal-name').text(element.data('name'));
            $('#modal-surname').text(element.data('surname'));
            $('#modal-phone').text(element.data('phone'));
            $('#modal-text').text(element.data('message'));
            $('#modal').modal('show');

            checked(element.attr('id'));
        }
    })

    function checked(id)
    {
        $.post( "/approve-feedback", {id: id} );

        $("#checked-" + id).text("Да");
    }
</script>