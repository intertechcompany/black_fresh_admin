<?php
/* @var $this SiteController */
?>
<?php
	$errors = $model->getPageErrors();
	$fields = $model->attributes;
?>
<?php if (!empty($errors)) { ?>
<div class="alert alert-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"><?=Yii::t('app', 'Close')?></span></button>
	<?=implode("<br>\n", $errors)?>
</div>
<?php } ?>
<div class="registration">
	<h2 class="text-center"><?=Yii::t('login', 'Log In h1')?></h2>
	<form method="post" role="form">
		<div class="form-group reg-inputs">
			<input type="text" class="form-control reg-name" name="login[mail]" value="<?=CHtml::encode($fields['mail'])?>" placeholder="<?=Yii::t('login', 'Login or email address input')?>">
			<input type="password" class="form-control reg-password" name="login[password]" value="<?=CHtml::encode($fields['password'])?>" placeholder="<?=Yii::t('login', 'Password input')?>">
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary btn-lg btn-block"><?=Yii::t('login', 'Log In btn')?></button>
		</div>
	</form>
</div>