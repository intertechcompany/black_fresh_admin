<?php
/* @var $this AdminController */
?>
<h1>Запрос телефонного звонка</h1>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover" style="cursor: pointer">
                <thead>
                    <tr id="main">
                        <th scope="col">#</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Обработано</th>
                        <th scope="col">Создано</th>
                        <th scope="col">Обновлено</th>
                        <th scope="col">Обработать</th>
                    </tr>
                </thead>
                <tbody>
                <?php if (!empty($phoneRequests)) { ?>
                    <?php foreach ($phoneRequests as $item) {?>
                        <tr>
                            <th scope="row"><?= $item['id'] ?></th>
                            <td><?= $item['name'] ?></td>
                            <td><?= $item['phone'] ?></td>
                            <td><?= $item['completed'] ? 'Да' : 'Нет' ?></td>
                            <td><?= $item['created'] ?></td>
                            <td><?= $item['updated'] ?></td>
                            <td>
                                <button
                                        class="btn approve-btn"
                                        data-id="<?= $item['id'] ?>"
                                >
                                    Обработать
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <?php if ($total > 1) { ?>
                <div class="pages text-center">
                    <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'pagination',
                        ),
                    ));
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    const tableTrs = document.getElementsByClassName('approve-btn');

    if (tableTrs) {
        Array.from(tableTrs).forEach((item) => {
            item.addEventListener('click', () => {
                $.post( "/approvePhoneRequest", {id: item.dataset.id} );
                setTimeout(() => {
                    location.reload();
                }, 300);
            })
        })
    }
</script>