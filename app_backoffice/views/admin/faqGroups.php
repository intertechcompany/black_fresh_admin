<?php
/* @var $this AdminController */
?>
<h1>FAQ Группы</h1>

<?php
	$page_url_data = array();

	if ($sort != 'default') {
		$page_url_data['sort'] = $sort;
		$page_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$page_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$page_url_data['page'] = $page + 1;
	}

	$page_new_url = $this->createUrl('faqGroupAdd');
?>
<p class="text-center"><a class="btn btn-success" href="<?=$page_new_url?>">Добавить</a></p>

<?php if (!empty($faq_list)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Заголовок</th>
				<th>Позиция</th>
				<th>Активный</th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($faq_list as $id => $item) { ?>
			<tr>
				<td>
					<?php echo $item['faq_id']; ?>
				</td>
				<td>
                    <?php echo $item['faq_title']; ?>
                </td>
				<td>
                    <?php echo $item['order']; ?>
				</td>
                <td>
                    <?php if ($item['active'] == 1) { ?>
                        Да
                    <?php } else { ?>
                        Нет
                    <?php } ?>
                </td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$item['faq_id']?>">
						<div class="btn-group">
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?php echo $this->createUrl('faqGroupEdit', array_merge(array('id' => $item['faq_id']))); ?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>
