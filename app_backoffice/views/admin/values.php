<?php
/* @var $this AdminController */
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>

<p class="text-center" style="padding-bottom: 20px">
	<a href="<?=$this->createUrl('properties')?>"><small><span class="glyphicon glyphicon-chevron-left"></span></small> <?=Yii::t('values', 'Back to the properties list')?></a>
</p>

<?php
	$value_url_data = array(
		'property_id' => $property['property_id'],
	);

	if ($sort != 'default') {
		$value_url_data['sort'] = $sort;
		$value_url_data['direction'] = $direction;
	}

	if (!empty($keyword)) {
		$value_url_data['keyword'] = $keyword;
	}

	if (!empty($page)) {
		$value_url_data['page'] = $page + 1;
	}

	$value_new_url = $this->createUrl('propertyvalue', array_merge(array('id' => 'new'), $value_url_data));

	$assetsUrl = Yii::app()->assetManager->getBaseUrl() . '/value';
?>
<p class="text-center"><a class="btn btn-success" href="<?=$value_new_url?>"><?=Yii::t('values', 'Add value btn')?></a></p>

<form class="search-form form-inline text-center" method="get">
	<div class="form-group">
		<input style="width: 250px;" class="form-control input-sm" type="text" name="keyword" placeholder="<?=Yii::t('values', 'ID | value title placeholder')?>" value="<?=CHtml::encode($keyword)?>">
		<button type="submit" class="btn btn-default btn-sm"><?=Yii::t('app', 'Search btn')?></button>
		<?php if ($sort != 'default' || !empty($keyword)) { ?>
		<br><a href="<?=$this->createUrl('propertyvalues',  array('id' => $property['property_id']))?>" style="display: inline-block; margin-top: 6px">&times;<small> <?=Yii::t('app', 'Reset search and sorting link')?></small></a>
		<?php } ?>
	</div>
</form>

<?php if (!empty($values)) { ?>
<p class="text-center"><strong><?=Yii::t('app', 'Total found')?>: <?=$total['total']?></strong></p>
<form id="manage-values" class="form-inline" method="post">
	<input id="entity-id" type="hidden" name="value_id" value="">
	<input id="entity-action" type="hidden" name="action" value="">
	
	<table class="table-data table table-striped">
		<thead>
			<tr>
				<?php
					if (!empty($keyword)) {
						$sort_data = array(
							'id' => $property['property_id'],
							'keyword' => $keyword,
						);
					} else {
						$sort_data = array(
							'id' => $property['property_id'],
						);
					}
				?>
				<th style="width: 4%"></th>
				<th style="width: 8%">
					<?php if ($sort == 'value_id' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_id', 'direction' => 'desc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'value_id' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_id', 'direction' => 'asc'), $sort_data))?>">ID</a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_id', 'direction' => 'asc'), $sort_data))?>">ID</a>
					<?php } ?>
				</th>
				<th>
					<?php if ($sort == 'value_title' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_title', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('values', 'Value title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'value_title' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('values', 'Value title col')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_title', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('values', 'Value title col')?></a>
					<?php } ?>
				</th>
				<th width="15%">
					<?php if ($sort == 'value_position' && $direction == 'asc') { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_position', 'direction' => 'desc'), $sort_data))?>"><?=Yii::t('values', 'Position')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes"></span></small>
					<?php } elseif ($sort == 'value_position' && $direction == 'desc') { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_position', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('values', 'Position')?></a> <small><span class="glyphicon glyphicon-sort-by-attributes-alt"></span></small>
					<?php } else { ?>
					<a href="<?=$this->createUrl('propertyvalues', array_merge(array('sort' => 'value_position', 'direction' => 'asc'), $sort_data))?>"><?=Yii::t('values', 'Position')?></a>
					<?php } ?>
				</th>
				<th width="14%"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($values as $id => $value) { ?>
			<?php
				$value_id = $value['value_id'];

				$value_url = $this->createUrl('propertyvalue', array_merge(array('id' => $value_id), $value_url_data));
			?>
			<tr>
				<td>
					<input type="checkbox" name="selected[]" value="<?=$value_id?>">
				</td>
				<td>
					<a href="<?=$value_url?>"><?=$value_id?></a>
				</td>
				<td>
					<a href="<?=$value_url?>"><?=CHtml::encode($value['value_title'])?></a>
					<?php if (!empty($value['value_top'])) { ?>
					&nbsp;<span class="label label-success">top</span>
					<?php } ?>
					<?php if (!empty($value['value_multicolor'])) { ?>
					<span class="value-multicolor"></span>
					<?php } elseif (!empty($value['value_color'])) { ?>
					<span class="value-color" style="background-color: <?=$value['value_color']?>"></span>
					<?php } ?>
				</td>
				<td>
					<input class="form-control input-sm text-center" type="text" name="property_value[<?=$value_id?>]" value="<?=CHtml::encode($value['value_position'])?>" style="width: 50px">
				</td>
				<td class="text-right" style="border-right: none;">
					<span class="edit-btns" data-id="<?=$value_id?>">
						<div class="btn-group">
							<a title="<?=Yii::t('app', 'Edit btn')?>" class="btn btn-default btn-sm" href="<?=$value_url?>" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-pencil"></span></a>
							<a title="<?=Yii::t('app', 'Delete btn')?>" class="delete-btn btn btn-default btn-sm" href="#" data-toggle="tooltip" data-placement="top"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</span>
				</td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr class="tBot">
				<td colspan="8">
					<div class="bulk-actions clearfix">
						<div class="form-group">
							<span class="check-toggle form-control-static input-sm"><span><?=Yii::t('app', 'Select all / Unselect all btn')?></span></span>
						</div>
						<div class="form-group">
							<select id="bulkAction" class="form-control input-sm" name="bulkAction">
								<option value="save"><?=Yii::t('app', 'Save positions option')?></option>
								<option value="delete"><?=Yii::t('app', 'Delete selected')?></option>
							</select>
							<strong id="topMsg"></strong>
						</div>
						<button class="btn btn-primary btn-sm pull-right" type="submit"><?=Yii::t('app', 'Apply btn')?></button>
					</div>
				</td>
			</tr>
		</tfoot>
	</table>
	<?php if ($total['pages'] > 1) { ?>
	<div class="pages text-center">
		<?php
			$this->widget('LinkPager', array(
				'pages' => $pages,
				'maxButtonCount' => 7,
				'htmlOptions' => array(
					'class' => 'pagination',
				),
			));
		?>
	</div>
	<?php } ?>
</form>
<?php } else { ?>
<p class="text-center"><?=Yii::t('app', 'No records found')?></p>
<?php } ?>

<script>
	$(document).ready(function(){
		var checkboxes = $(".table-data input[type=checkbox]"),
			submit_form = false;
		
		$(".delete-btn").click( function(){
			var that = $(this);
			
			bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete?')?>", function(result) {
				if (result) {
					submit_form = true;

					$('#entity-action').val('delete');
					$('#entity-id').val(that.parent().parent().attr("data-id"));
					$('#manage-values').submit();
				}
			});
			
			return false;
		});
		
		$(".check-toggle").click( function(){
			if($(this).hasClass("checked"))
			{
				checkboxes.prop('checked', false);
			}
			else
			{
				checkboxes.prop('checked', true);
			}
			
			$(this).toggleClass("checked");
		});
		
		$("#manage-values").submit(function() {
			if (submit_form) {
				return true;
			}
			
			if($("#bulkAction").val() != 'save' && !$(this).find(".table-data input[type=checkbox]:checked").length)
				return false;
			
			if ($("#bulkAction").val() == 'delete') {
				var that = $(this);

				bootbox.confirm("<?=Yii::t('app', 'Are you sure you want to delete selected items?')?>", function(result) {
					if (result) {
						submit_form = true;
						that.submit();
					}
				});
				
				return false;
			}
		});
	});	
</script>