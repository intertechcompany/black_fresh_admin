<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = 'Error ' . $code;
$this->pageDescription = '';
$this->pageKeywords = '';

$this->breadcrumbs=array(
	$this->pageTitle,
);
?>
<h1><?=CHtml::encode($this->pageTitle)?></h1>
<p><?php echo CHtml::encode($message); ?></p>