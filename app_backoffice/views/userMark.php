<?php
/* @var $this AdminController */
?>
<h1>Оценки подписок</h1>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-hover" style="cursor: pointer">
                <thead>
                    <tr id="main">
                        <th scope="col">#</th>
                        <th scope="col"><?=Lang::t('feedback.page.user')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.mark')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.product')?></th>
                        <th scope="col"><?=Lang::t('feedback.page.created')?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $item) {?>
                    <tr id="<?php echo $item['mark']['id'] ?>"
                        data-id="<?php echo $item['mark']['id'] ?>"
                        data-answ1="<?php echo $item['answers'][0]['title_ru']; ?>"
                        data-answ2="<?php echo $item['answers'][1]['title_ru']; ?>"
                        data-answ3="<?php echo $item['answers'][2]['title_ru']; ?>"
                        data-text1="<?php echo $item['mark']['text_1']; ?>"
                        data-text2="<?php echo $item['mark']['text_2']; ?>"
                        data-text3="<?php echo $item['mark']['text_3']; ?>"
                        <?php if ($item['mark']['checked'] == false) { echo "class='bg-success'"; } ?>
                    >
                        <th><?php echo $item['mark']['id'] ?></th>
                        <th scope="row"><?php echo $item['user']['user_first_name'] . ' ' .  $item['user']['user_last_name'];?></th>
                        <th
                            <?php if ($item['mark']['is_good'] == true) { echo "style='color: green;'"; } else { echo "style='color: red;'"; } ?>
                        ><?php if ($item['mark']['is_good'] == true) { echo "Нравится"; } else { echo "Не нравится"; } ?></th>
                        <td><a href="<?php echo $this->createUrl('product', ['id' => $item['mark']['product_id']]); ?>" target="_blank">Товар</a></td>
                        <td><?php echo $item['mark']['created'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php if ($total > 1) { ?>
                <div class="pages text-center">
                    <?php
                    $this->widget('LinkPager', array(
                        'pages' => $pages,
                        'maxButtonCount' => 7,
                        'htmlOptions' => array(
                            'class' => 'pagination',
                        ),
                    ));
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-sm" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Причины</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover" style="cursor: pointer">
                    <tbody>
                    <tr>
                        <td>Причина 1</td>
                        <td id="reason_1"></td>
                    </tr>
                    <tr>
                        <td>Причина 2</td>
                        <td id="reason_2"></td>
                    </tr>
                    <tr>
                        <td>Причина 3</td>
                        <td id="reason_3"></td>
                    </tr>
                    <tr>
                        <td>Свой ответ 1</td>
                        <td id="text_1"></td>
                    </tr>
                    <tr>
                        <td>Свой ответ 2</td>
                        <td id="text_2"></td>
                    </tr>
                    <tr>
                        <td>Свой ответ 3</td>
                        <td id="text_3"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=Lang::t('subscription.page.btn.close')?></button>
                <button type="button" id="approve-btn" onclick="update()" data-id="" class="btn btn-primary"><?=Lang::t('subscription.page.btn.approve')?></button>
            </div>
        </div>
    </div>
</div>

<script>
    $('tr').click(function () {
        const element = $("#" + this.id);

        if (element.attr('id') !== 'main') {
            $('#reason_1').text(element.data('answ1'));
            $('#reason_2').text(element.data('answ2'));
            $('#reason_3').text(element.data('answ3'));
            $('#text_1').text(element.data('text1'));
            $('#text_2').text(element.data('text2'));
            $('#text_3').text(element.data('text3'));

            $('#approve-btn').attr('data-id', this.id);
            $('#modal').modal('show');
        }
    });

    function update() {
        let id = $('#approve-btn').attr('data-id');
        let data = {};
        data.mark =  id;

        document.getElementById(id).className = '';

        $.post( "/approve-mark", data );
    }
</script>