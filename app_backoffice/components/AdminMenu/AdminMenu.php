<?php
class AdminMenu extends CWidget
{ 
    public function run()
    {
        $cookie = Yii::app()->request->cookies['new_login_2'];

        if (empty($cookie->value)) {
            Yii::app()->user->logout(true);
        }

		$new_orders = Order::model()->getNewOrdersTotal();
		$new_requests = Request::model()->getNewRequestsTotal();

		$newSub = Subscription::model()->getCountNew();
		$newFeedback = Feedback::model()->getCountNew();
		$newUserFeedback = UserFeedback::model()->getCountNew();

		$this->render('adminMenu', array(
			'new_orders' => $new_orders,
			'new_requests' => $new_requests,
            'newSub' => $newSub,
            'newFeedback' => $newFeedback,
            'newUserFeedback' => $newUserFeedback,
		));
    }
}
