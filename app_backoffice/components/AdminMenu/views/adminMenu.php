<?php
  $_c = Yii::app()->getController();
  $user_name = Yii::app()->user->name;
?>
<style>
    .top-menu .nav > li {
        font-size: 12px;
    }
</style>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="top-menu">
<ul class="nav navbar-nav">
  	<li class="dropdown">
  		<a id="dropdownMenu1" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Catalog menu')?><?php if (!empty($new_orders)) { ?> <span class="badge"><?=$new_orders?></span><?php } ?> <span class="caret"></span></a>
  		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('orders')?>"><?=Yii::t('app', 'Orders menu')?><?php if (!empty($new_orders)) { ?> <span class="badge"><?=$new_orders?></span><?php } ?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('categories')?>"><?=Yii::t('app', 'Categories menu')?></a></li>
  			<!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('brands')?>"><?=Yii::t('app', 'Brands menu')?></a></li> -->
  			<!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('collections')?>"><?=Yii::t('app', 'Collections menu')?></a></li> -->
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('products')?>"><?=Yii::t('app', 'Products menu')?></a></li>
  			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('discounts')?>">Промокоды</a></li>
        	<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('properties')?>"><?=Yii::t('app', 'Properties menu')?></a></li>
  		</ul>
  	</li>
	<li class="dropdown">
  		<a id="dropdownMenu12" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'School menu')?><?php if (!empty($new_requests)) { ?> <span class="badge"><?=$new_requests?></span><?php } ?> <span class="caret"></span></a>
  		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu12">
		    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('requests')?>"><?=Yii::t('app', 'Registrations menu')?><?php if (!empty($new_requests)) { ?> <span class="badge"><?=$new_requests?></span><?php } ?></a></li>
		    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('authors')?>"><?=Yii::t('app', 'Lectors menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('courses')?>"><?=Yii::t('app', 'Courses menu')?></a></li>
		</ul>
  	</li>
	<li class="dropdown">
  		<a id="dropdownMenu11" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Articles menu')?> <span class="caret"></span></a>
  		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu11">
		    <li class="dropdown-header">Новости</li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('blogcategories')?>"><?=Yii::t('app', 'Categories menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('blogs')?>"><?=Yii::t('app', 'Blog menu')?></a></li>
			<li class="divider"></li>
			<li class="dropdown-header">База знаний</li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('basecategories')?>"><?=Yii::t('app', 'Categories menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('bases')?>"><?=Yii::t('app', 'Knowledge base menu')?></a></li>
		</ul>
	</li>
	<li class="dropdown">
  		<a id="dropdownMenu13" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Pages menu')?> <span class="caret"></span></a>
  		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu13">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('members')?>"><?=Yii::t('app', 'Members menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('pages')?>"><?=Yii::t('app', 'Pages menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('faqGroup')?>">FAQ группы</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('faqQuest')?>">FAQ вопросы</a></li>
		</ul>
	</li>
  	<li><a href="<?=$_c->createUrl('banners')?>"><?=Yii::t('app', 'Banners menu')?></a></li>
	<!-- <li><a href="<?=$_c->createUrl('users')?>"><?=Yii::t('app', 'Users menu')?></a></li> -->
	<li class="dropdown">
	<a id="dropdownMenu2" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Users menu')?> <span class="caret"></span></a>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('users')?>"><?=Yii::t('app', 'Clients menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('managers')?>"><?=Yii::t('app', 'Managers menu')?> <span class="badge">beta</span></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('roles')?>"><?=Yii::t('app', 'Roles menu')?> <span class="badge">beta</span></a></li>
		</ul>
	</li>
    <li class="dropdown">
        <a id="dropdownMenu3" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
            Feedback
            <?php
                $total = 0;

                if ($newFeedback['total'] > 0) {
                    $total += $newFeedback['total'];
                }

                if ($newUserFeedback['total'] > 0) {
                    $total += $newUserFeedback['total'];
                }

                if ($total > 0) {
            ?>
            <span class="badge">
                <?php
                    echo $total;
                ?>
            </span>
            <?php } ?>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
            <li role="presentation">
                <a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('feedback')?>" aria-expanded="true">Feedback Form <span class="badge"><?php if ($newFeedback['total'] > 0) { echo $newFeedback['total']; } ?></span></span></a>
            </li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('userfeedback')?>">User Feedback <span class="badge"><?php if ($newUserFeedback['total'] > 0) { echo $newUserFeedback['total']; } ?></span></a></li>
        </ul>
    </li>
    <li>
        <a id="dropdownMenu22" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">Подписка <span class="badge"><?php if ($newSub['total'] > 0) { echo $newSub['total']; } ?></span> <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu21">
            <li role="presentation"><a role="menuitem" tabindex="-1"  href="<?=$_c->createUrl('subscription')?>" aria-expanded="true">Подписки <span class="badge"><?php if ($newSub['total'] > 0) { echo $newSub['total']; } ?></span></span></a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('usermark')?>">Оценки</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('exportSubCsv')?>">Выгрузка подписок</a></li>
        </ul>
    </li>
	<li class="dropdown">
		<a id="dropdownMenu21" class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"><?=Yii::t('app', 'Settings menu')?> <span class="caret"></span></a>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu21">
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('translations')?>"><?=Yii::t('app', 'Translations menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('settings')?>"><?=Yii::t('app', 'Settings menu')?></a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('updateNpCities')?>">Обновить города НП</a></li>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?=$_c->createUrl('updateNpDepartment')?>">Обновить отделения НП</a></li>
		</ul>
	</li>
  	<li><a href="<?=$_c->createUrl('logout')?>"><?=Yii::t('app', 'Logout menu')?></a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
	<li><a target="_blank" href="<?=Yii::app()->params->url?>"><?=Yii::t('app', 'Go to the website')?></a></li>
</ul>
</div><!-- /.navbar-collapse -->