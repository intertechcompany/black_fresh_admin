<?php


class Export
{
    private $fileName;

    public const PER_ITERATION = 5000;

    public $exportPromo = [
        'ID',
        'ID заказа',
        'Пользователь',
        'Подтвержденная подписка',
        'Частота доставки',
        'Цена',
        'Товар',
        'Количество',
        'Адрес',
        'Дата создания',
    ];

    public function __construct($fileName)
    {
        $this->fileName = $fileName . '.csv';
    }

    public function exportSub()
    {
        $data = $this->getSubData();

        return [
            'fileName' => $this->fileName,
            'data' => $data,
        ];
    }

    private function getSubData()
    {
        $subData = [];
        $subData[] = $this->exportPromo;

        $subCount = Subscription::model()->getSubOrderCount();

        $count = (int) $subCount['total'] / self::PER_ITERATION;

        for ($i = 0; $i < $count; $i++) {
            $offset = self::PER_ITERATION * $i;
            $subOrders = Subscription::model()->getExportData($offset, self::PER_ITERATION);

            foreach ($subOrders as $order) {

                $freqText = '';

                if ((int)$order['frequency_id'] === 1) {
                    $freqText =  'Раз в неделю';
                } elseif ((int)$order['frequency_id'] === 2) {
                    $freqText =  'Раз в две недели';
                } elseif ((int)$order['frequency_id'] === 3) {
                    $freqText =  'Раз в три недели';
                } elseif ((int)$order['frequency_id'] === 4) {
                    $freqText =  'Раз в четыре недели';
                }

                $subData[] = [
                    $order['id'],
                    $order['order_reference'],
                    $order['user']['user_first_name'] . " " . $order['user']['user_last_name'],
                    $order['payment_status'] !== 'Approved' ? 'Нет' : 'Да',
                    $freqText,
                    $order['price'],
                    $order['product']['title'],
                    $order['product']['quantity'],
                    $order['np_city'] . " " . $order['np_department'] . " " . $order['delivery_address'],
                    $order['created'],
                ];
            }
        }

        return $subData;
    }
}