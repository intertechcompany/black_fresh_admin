<?php


class NovaPoshta
{
    public const BASE_API_URL = 'https://api.novaposhta.ua/v2.0/json/';

    private $apiKey;

    public function __construct()
    {
        $this->apiKey = Yii::app()->params['np']['api_key'];
    }

    public function updateNpCities(): void
    {
        $data = [
            "modelName" => "Address",
            "calledMethod" => "getCities",
            "methodProperties" => [
                'Page' => 1
            ],
            "apiKey" => $this->apiKey
        ];

        $result = $this->send($data);

        if ($result) {
            $this->saveCities($result['data']);
        }
    }

    public function updateNpDepartment()
    {
        $requestParams = [
            "modelName" => "AddressGeneral",
            "calledMethod" => "getWarehouses",
            "methodProperties" => [
                'Page' => 1,
                'Limit' => 500
            ],
            "apiKey" => $this->apiKey
        ];

        $maxPageCount = $this->getFirstPageNpDepartment($requestParams);

        if ($maxPageCount > 1) {
            $this->getAnotherPageNpDepartment($maxPageCount, $requestParams);
        }
    }

    /**
     * @param $requestParams
     * @return int
     */
    private function getFirstPageNpDepartment($requestParams): int
    {
        $maxPageCount = 1;

        $response = $this->send($requestParams);

        if ($response) {
            if (isset($response['info']['totalCount'])) {
                $maxPageCount = round((int)$response['info']['totalCount'] / 500);
            }
            $this->saveDepartments($response['data']);
        }

        return $maxPageCount;
    }

    /**
     * @param $maxPageCount
     * @param $requestParams
     */
    private function getAnotherPageNpDepartment($maxPageCount, $requestParams): void
    {
        for ($page = 2; $page < $maxPageCount; $page++) {
            $requestParams['methodProperties']['Page'] = $page;
            $response = $this->send($requestParams);

            if ($response) {
                $this->saveDepartments($response['data']);
            }
        }
    }

    private function send($data, $url = '')
    {
        $data = json_encode($data);

        $curl = curl_init(self::BASE_API_URL . $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            ]
        );

        $result = curl_exec($curl);
        $responseCode = curl_errno($curl);
        $error = curl_error($curl);
        curl_close($curl);

        if ($responseCode > 0) {
            return false;
        }

        return json_decode($result, true);
    }

    private function saveCities($data): void
    {
        foreach ($data as $cityNp) {
            $city = Yii::app()->db
                ->createCommand("SELECT COUNT(*) as total FROM np_city WHERE city_ref = :ref")
                ->bindValue(':ref', (string)$cityNp['Ref'])
                ->queryScalar();

            if ((int)$city === 0) {
                $this->createCity($cityNp);
            } else {
                $this->updateCity($cityNp);
            }
        }
    }

    private function createCity($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $now = date('Y-m-d H:i:s');

        $insert = [
            'city_name' => $data['Description'],
            'city_name_ru' => $data['DescriptionRu'],
            'city_ref' => $data['Ref'],
            'city_id' => $data['CityID'],
            'created' => $now,
            'saved' => $now,
        ];

        $rs = $builder->createInsertCommand('np_city', $insert)->execute();
    }

    private function updateCity($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $now = date('Y-m-d H:i:s');

        $update = [
            'city_name' => $data['Description'],
            'city_name_ru' => $data['DescriptionRu'],
            'city_id' => $data['CityID'],
            'saved' => $now,
        ];

        $criteria = new CDbCriteria(
            [
                "condition" => "city_ref = :ref",
                "params" => [
                    "ref" => $data['Ref'],
                ]
            ]
        );

        $rs = $builder->createUpdateCommand('np_city', $update, $criteria)->execute();
    }

    private function saveDepartments($data): void
    {
        foreach ($data as $departmentNp) {
            $department = Yii::app()->db
                ->createCommand("SELECT COUNT(*) as total FROM np_department WHERE department_ref = :ref")
                ->bindValue(':ref', (string)$departmentNp['Ref'])
                ->queryScalar();

            if ((int)$department === 0) {
                $this->createDepartment($departmentNp);
            } else {
                $this->updateDepartment($departmentNp);
            }
        }
    }

    private function createDepartment($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $now = date('Y-m-d H:i:s');

        $insert = [
            'department_name' => $data['Description'],
            'department_name_ru' => $data['DescriptionRu'],
            'department_key' => $data['SiteKey'],
            'department_num' => $data['Number'],
            'department_ref' => $data['Ref'],
            'city_ref' => $data['CityRef'],
            'created' => $now,
            'saved' => $now,
        ];

        $rs = $builder->createInsertCommand('np_department', $insert)->execute();
    }

    private function updateDepartment($data)
    {
        $builder = Yii::app()->db->schema->commandBuilder;
        $now = date('Y-m-d H:i:s');

        $update = [
            'department_name' => $data['Description'],
            'department_name_ru' => $data['DescriptionRu'],
            'department_key' => $data['SiteKey'],
            'department_num' => $data['Number'],
            'city_ref' => $data['CityRef'],
            'saved' => $now,
        ];

        $criteria = new CDbCriteria(
            [
                "condition" => "department_ref = :ref",
                "params" => [
                    "ref" => $data['Ref'],
                ]
            ]
        );

        $rs = $builder->createUpdateCommand('np_department', $update, $criteria)->execute();
    }
}