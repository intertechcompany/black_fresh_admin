<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$protectedPath = realpath(Yii::getPathOfAlias('root') . DS . 'app_backoffice');
$runtimePath   = realpath(Yii::getPathOfAlias('root') . DS . 'runtime');
$sessionPath   = realpath(Yii::getPathOfAlias('root') . DS . 'tmp');
$assetsPath    = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'www' . DS . 'assets');

return array(
	'basePath'=>$protectedPath,
	'id'=>'black',
	'name'=>'Black',
	'charset'=>'utf-8',
	'defaultController'=>'admin',
	
	'language'=>'ru',
	'sourceLanguage'=>'en',
	'timeZone'=>'Europe/Kiev', // UTC
	
	'runtimePath' => $runtimePath,

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.Forms.*',
		'application.components.*',
	),

	// application components
	'components'=>array(
		'assetManager'=>array(
			'basePath'=>$assetsPath,
			'baseUrl'=>'http://black.loc/assets',
		),
		'format'=>array(
			'class'=>'CLocalizedFormatter',
		),
		'messages'=>array(
			'class'=>'CPhpMessageSource',
		),
		'user'=>array(
			'class'=>'application.components.WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'autoRenewCookie'=>true,
			'identityCookie'=>array(
				'path' => '/',
				'domain' => 'backoffice.black.loc',
				'httpOnly' => true,
			),
			'loginUrl'=>array('admin/login'),
		),
		'urlManager'=>array(
			'class'=>'application.components.UrlManager',
			'actAddressedDoubleUrl'=>'404',
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>true,
			'useStrictParsing'=>true,
			'routeVar'=>'_r',
			'rules'=>array(
				''=>'admin/index',
				'ajax/<_a:\w+>'=>'ajax/<_a>',
				'<_type:(login|logout|categories|brands|collections|properties|products|discounts|orders|courses|authors|members|requests|blogcategories|blogs|basecategories|bases|banners|pages|users|managers|roles|translations|settings|sandbox)>'=>'admin/<_type>',
				'<_type:(category|brand|collection|property|product|discount|order|course|author|member|request|blogcategory|blog|basecategory|base|banner|page|user|manager|role|translation)>/<id:(\d+|new)>'=>'admin/<_type>',
				'property-values/<id:(\d+)>'=>'admin/propertyvalues',
				'property-value/<property_id:(\d+)>/<id:(\d+|new)>'=>'admin/propertyvalue',
			),
		),
		'db'=>array(
            'connectionString'=>'mysql:host=mysql_dev;dbname=fresh;port=3307',
            'emulatePrepare'=>true,
            'username'=>'fresh',
            'password'=>'fresh',
			'charset'=>'utf8',
			'autoConnect'=>false,
			// включаем профайлер
			'enableProfiling'=>true,
			// показываем значения параметров
			'enableParamLogging' => true,
		),
		'session'=>array(
			//'class'=>'CCacheHttpSession',
			'class'=>'CHttpSession',
			'autoStart'=>true,
			'cookieMode'=>'only',
			'sessionName'=>'bSessID',
			'savePath'=>$sessionPath,
			'gCProbability'=>1,
			'timeout'=>7200,
			'cookieParams' => array(
				'path' => '/',
				'domain' => 'backoffice.black.loc',
				'httpOnly' => true,
			),
		),
		'widgetFactory'=>array(
			'widgets'=>array(
				'LinkPager'=>array(
					'cssFile'              => false,
					'header'               => '',
					'firstPageCssClass'    => 'page-first',
					'lastPageCssClass'     => 'page-last',
					'previousPageCssClass' => 'page-prev',
					'nextPageCssClass'     => 'page-next',
					'internalPageCssClass' => false,
					// 'prevPageLabel'        => '<',
					// 'nextPageLabel'        => '>',
					// 'firstPageLabel'       => '<<',
					// 'lastPageLabel'        => '>>',
				),
			),
		),
		'errorHandler'=>array(
			'errorAction'=>'admin/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, info',
					'except'=>'exception.CHttpException.*',
				),
				// uncomment the following to show log messages on web pages
				array(
					'class'=>'CWebLogRoute',
					//'categories' => 'application',
					'levels'=>'error, warning, trace, profile, info',
				),
				array(
					// направляем результаты профайлинга в ProfileLogRoute (отображается внизу страницы)
					'class'=>'CProfileLogRoute',
					'levels'=>'profile',
				),
			),
		),
		'cache'=>array(
			'class'=>'CDummyCache',
		),
	),
	
	'onBeginRequest' => array('BeginRequest', 'onStartSite'),
	// 'onEndRequest' => array('BeginRequest', 'onStopSite'),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'dev' => true,
		'secret' => 'bg7,{Fk',
		'url' => 'http://black.loc/',
		'currencies' => [
			'usd' => [
				'code' => 'usd',
				'name' => 'USD',
				'suffix' => '',
				'prefix' => '$',
			],
			'eur' => [
				'code' => 'eur',
				'name' => 'EUR',
				'suffix' => '€',
				'prefix' => '',
			],
			'uah' => [
				'code' => 'uah',
				'name' => 'Грн.',
				'suffix' => ' грн.',
				'prefix' => '',
			],
		],
		'lang' => 'uk',
		'langs' => array(
			'uk' => 'Українська',
			'ru' => 'Русский',
			'en' => 'Английский',
		),
		'translations' => array(),
		'mailer' => array(
			'from_email' => 'noreply@fresh.black',
			'from_name' => 'Black',
			'sendgrid' => array(
				'api_key' => '*****',
			),
			'mailgun' => array(
				'domain' => 'mailer.fresh.black',
				'api_key' => 'key-*****',
			),
		),
        'np' => [
            'api_key' => 'f892ab0bd4f124685cd2a7a535fcad5f',
        ],
	),
);
