<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

$protectedPath = realpath(Yii::getPathOfAlias('root') . DS . 'app_backoffice');
$runtimePath   = realpath(Yii::getPathOfAlias('root') . DS . 'runtime');
$sessionPath   = realpath(Yii::getPathOfAlias('root') . DS . 'tmp');
$assetsPath    = realpath(Yii::getPathOfAlias('system') . DS . '..' . DS . 'www' . DS . 'assets');


return array(
	'basePath'=>$protectedPath,
	'id'=>'black',
	'name'=>'Black',
	'charset'=>'utf-8',
	'defaultController'=>'admin',
	
	'language'=>'ru',
	'sourceLanguage'=>'en',
	'timeZone'=>'Europe/Kiev',
	
	'runtimePath' => $runtimePath,

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.Forms.*',
		'application.components.*',
	),

	// application components
	'components'=>array(
		'assetManager'=>array(
			'basePath'=>$assetsPath,
			'baseUrl'=>'https://fresh.black/assets',
		),
		'format'=>array(
			'class'=>'CLocalizedFormatter',
		),
		'messages'=>array(
			'class'=>'CPhpMessageSource',
		),
		'user'=>array(
			'class'=>'application.components.WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'autoRenewCookie'=>true,
			'identityCookie'=>array(
				'path' => '/',
				'domain' => 'backoffice.fresh.black',
				'secure' => true,
				'httpOnly' => true,
			),
			'loginUrl'=>array('admin/login'),
		),
		'urlManager'=>array(
			'class'=>'application.components.UrlManager',
			'actAddressedDoubleUrl'=>'404',
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>true,
			'useStrictParsing'=>true,
			'routeVar'=>'_r',
			'rules'=>array(
				''=>'admin/index',
				'ajax/<_a:\w+>'=>'ajax/<_a>',
				'<_type:(login|logout|categories|brands|collections|properties|products|discounts|orders|courses|authors|members|requests|blogcategories|blogs|basecategories|bases|banners|pages|users|managers|roles|translations|settings|feedback|userfeedback|usermark|sandbox|faqGroup|faqGroupEdit|faqGroupAdd|faqQuest|faqQuestAdd|faqQuestEdit|updateNpCities|updateNpDepartment|exportSubCsv)>'=>'admin/<_type>',
				'<_type:(category|brand|collection|property|product|discount|order|course|author|member|request|blogcategory|blog|basecategory|base|banner|page|user|manager|role|translation)>/<id:(\d+|new)>'=>'admin/<_type>',
				'property-values/<id:(\d+)>'=>'admin/propertyvalues',
				'property-value/<property_id:(\d+)>/<id:(\d+|new)>'=>'admin/propertyvalue',
                'subscription'=>'admin/subscription',
                'approve-sub' => 'admin/subscriptionApprove',
                'approve-feedback' => 'admin/feedbackApprove',
                'approve-userFeedback' => 'admin/userFeedbackApprove',
                'approve-mark' => 'admin/subscriptionApproveMark'
			),
		),
		'db'=>array(
			'connectionString'=>'mysql:host=localhost;dbname=nuare_black',
			'emulatePrepare'=>true,
			'username'=>'nuare_black',
			'password'=>'UmZIE8JPS1P6XiJX',
			'charset'=>'utf8',
			'autoConnect'=>false,
		),
		'session'=>array(
			// 'class'=>'CCacheHttpSession',
			'class'=>'CHttpSession',
			'autoStart'=>true,
			'cookieMode'=>'only',
			'sessionName'=>'bSessID',
			'savePath'=>$sessionPath,
			'gCProbability'=>1,
			'timeout'=>7200,
			'cookieParams' => array(
				'path' => '/',
				'domain' => 'backoffice.fresh.black',
				'secure' => true,
				'httpOnly' => true,
			),
		),
		'widgetFactory'=>array(
			'widgets'=>array(
				'LinkPager'=>array(
					'cssFile'              => false,
					'header'               => '',
					'firstPageCssClass'    => 'page-first',
					'lastPageCssClass'     => 'page-last',
					'previousPageCssClass' => 'page-prev',
					'nextPageCssClass'     => 'page-next',
					'internalPageCssClass' => false,
					// 'prevPageLabel'        => '<',
					// 'nextPageLabel'        => '>',
					// 'firstPageLabel'       => '<<',
					// 'lastPageLabel'        => '>>',
				),
			),
		),
		'errorHandler'=>array(
			'errorAction'=>'admin/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
					'except'=>'exception.CHttpException.*',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
					//'categories' => 'application',
					'levels'=>'error, warning, trace, profile, info',
				),
				array(
					// направляем результаты профайлинга в ProfileLogRoute (отображается внизу страницы)
					'class'=>'CProfileLogRoute',
					'levels'=>'profile',
				),
				*/
			),
		),
		'cache'=>array(
			// 'class'=>'CMemCache',
			'class'=>'CDummyCache',
		),
	),
	
	'onBeginRequest' => array('BeginRequest', 'onStartSite'),
	//'onEndRequest' => array('BeginRequest', 'onStopSite'),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'dev' => false,
		'secret' => 'bg7,{Fk',
		'url' => 'https://fresh.black/',
		'currencies' => [
			'usd' => [
				'code' => 'usd',
				'name' => 'USD',
				'suffix' => '',
				'prefix' => '$',
			],
			'eur' => [
				'code' => 'eur',
				'name' => 'EUR',
				'suffix' => '€',
				'prefix' => '',
			],
			'uah' => [
				'code' => 'uah',
				'name' => 'Грн.',
				'suffix' => ' грн.',
				'prefix' => '',
			],
		],
		'lang' => 'uk',
		'langs' => array(
			'uk' => 'Українська',
			'ru' => 'Русский',
			'en' => 'Английский',
		),
		'translations' => array(),
		'mailer' => array(
			'from_email' => 'noreply@fresh.black',
			'from_name' => 'Black',
			'sendgrid' => array(
				'api_key' => '*****',
			),
			'mailgun' => array(
				'domain' => 'mailer.fresh.black',
				'api_key' => 'key-*****',
			),
		),
		'np' => [
            'api_key' => 'f892ab0bd4f124685cd2a7a535fcad5f',
        ],
	),
);
