<?php
	return array(
		'Translation has been deleted! success' => 'Translation has been deleted!',
		'Translations have been deleted! success' => 'Translations have been deleted!',
		'Translations h1' => 'Translations',
		'Translation has been successfully added! success' => 'Translation has been successfully added!',
		'Translation has been successfully saved! success' => 'Translation has been successfully saved!',
		'Add translation h1' => 'Add translation',
		'Edit translation h1' => 'Edit translation',
		'Add translation btn' => 'Add translation',
		'ID | translation code placeholder' => 'ID | translation code | part of the phrase...',
		'Group:' => 'Group:',
		'All' => 'All',
		'Translation code col' => 'Code',
		'Translation value col' => 'Translation',
		'Translation code' => 'Translation code',
		'Translation code tip' => '<strong>Do not change without the need!</strong><br>Code format: <strong>group.entityType.translationCode</strong>, where:<br>
			<span style="display: block; margin: 6px 0;"><strong>group [required]</strong> &mdash; group identifier. For instance, page title (for articles page &mdash; blog), global identifier (for entities that exist in any language version and any regional settings &mdash; global), action (for dynamic AJAX requests &mdash; ajax)</span>
			<span style="display: block; margin: 6px 0;"><strong>entityType [not required]</strong> &mdash; type of translation. For instance, for button &mdash; btn; for link &mdash; link; for tip &mdash; tip; for HTML block &mdash; html; etc.</span>
			<span style="display: block; margin: 6px 0;"><strong>translationCode [required]</strong> &mdash; translation identifier (for instance, message &laquo;invalid email&raquo; can be identified as invalidEmail)</span>',
		'Translation tip' => 'Translation tip',
		'Translation type' => 'Translation type',
		'Type text' => 'Text',
		'Translation value' => 'Translation',
		'Enter a translation code!' => 'Enter a translation code!',
		'Enter a translation value!' => 'Enter a translation value!',
		'Invalid translation type!' => 'Invalid translation type!',
		'Enter a translation value in base language ({lang})!' => 'Enter a translation value in base language ({lang})!',
	);