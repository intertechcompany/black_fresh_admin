<?php
define('DS', DIRECTORY_SEPARATOR);

if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'backoffice.black.loc') {
	error_reporting(E_ALL);
	
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

	$yii = __DIR__ . DS . '..' . DS . '..' . DS . 'black_fresh' . DS . 'framework' . DS . 'yiilite.php';
	require_once $yii;

	Yii::setPathOfAlias('root', realpath(__DIR__ . DS . '..' . DS));

	$config = require_once __DIR__ . DS . '..' . DS . 'app_backoffice' . DS . 'config' . DS . 'backoffice-local.php';
}
else {

	$yii = __DIR__ . DS . '..' . DS . '..' . DS . '..' . DS . 'fresh.black' . DS . 'project' . DS . 'framework' . DS . 'yiilite.php';

	require_once $yii;

	Yii::setPathOfAlias('root', realpath(__DIR__ . DS . '..' . DS));
	
	$config = require_once __DIR__ . DS . '..' . DS . 'app_backoffice' . DS . 'config' . DS . 'backoffice.php';
}

Yii::$enableIncludePath = false; // ��������� PHP include path
Yii::createWebApplication($config)->run();
